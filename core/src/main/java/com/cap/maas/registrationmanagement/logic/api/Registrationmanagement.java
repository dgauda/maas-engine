package com.cap.maas.registrationmanagement.logic.api;

import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.cap.maas.registrationmanagement.logic.api.to.RegistrationCto;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationEto;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Interface for Registrationmanagement component.
 */
public interface Registrationmanagement {

  /**
   * Returns a Registration by its id 'id'.
   *
   * @param id The id 'id' of the Registration.
   * @return The {@link RegistrationEto} with id 'id'
   */
  RegistrationEto findRegistration(Long id);

  /**
   * Returns a paginated list of Registrations matching the search criteria.
   *
   * @param criteria the {@link RegistrationSearchCriteriaTo}.
   * @return the {@link List} of matching {@link RegistrationEto}s.
   */
  PaginatedListTo<RegistrationEto> findRegistrationEtos(RegistrationSearchCriteriaTo criteria);

  /**
   * Deletes a registration from the database by its id 'registrationId'.
   *
   * @param registrationId Id of the registration to delete
   * @return boolean <code>true</code> if the registration can be deleted, <code>false</code> otherwise
   */
  boolean deleteRegistration(Long registrationId);

  /**
   * Saves a registration and store it in the database.
   *
   * @param registration the {@link RegistrationEto} to create.
   * @return the new {@link RegistrationEto} that has been saved with ID and version.
   */
  RegistrationEto saveRegistration(RegistrationEto registration);

  /**
   * Returns a composite Registration by its id 'id'.
   *
   * @param id The id 'id' of the Registration.
   * @return The {@link RegistrationCto} with id 'id'
   */
  RegistrationCto findRegistrationCto(Long id);

  /**
   * Returns a paginated list of composite Registrations matching the search criteria.
   *
   * @param criteria the {@link RegistrationSearchCriteriaTo}.
   * @return the {@link List} of matching {@link RegistrationCto}s.
   */
  PaginatedListTo<RegistrationCto> findRegistrationCtos(RegistrationSearchCriteriaTo criteria);

  List<RegistrationEto> showRegisteredServices(Long id, Double latitude, Double longitude);
  
  List<RegistrationEto> getAllRegisteredServices(long typeId, String country, String city, String area);
  
  Set<String> getAllCountries(long typeId);
  
  Set<String> getAllCities(long typeId, String country);
  
  Set<String> getAllAreas(long typeId, String country, String city);  


}
