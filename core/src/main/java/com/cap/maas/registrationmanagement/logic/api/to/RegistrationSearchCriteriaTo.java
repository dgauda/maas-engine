package com.cap.maas.registrationmanagement.logic.api.to;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria} {@link net.sf.mmm.util.transferobject.api.TransferObject TO}
 * used to find {@link com.cap.maas.registrationmanagement.common.api.Registration}s.
 *
 */
public class RegistrationSearchCriteriaTo extends SearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private String name;

  private Long typeId;

  private Double latitude;

  private Double longitude;

  private String serviceUrl;
  
  private String country;
  
  private String city;
  
  private String area;

  /**
   * The constructor.
   */
  public RegistrationSearchCriteriaTo() {

    super();
  }

  public String getName() {

    return name;
  }

  public void setName(String name) {

    this.name = name;
  }

  public Long getTypeId() {

    return typeId;
  }

  public void setTypeId(Long typeId) {

    this.typeId = typeId;
  }

  public Double getLatitude() {

    return latitude;
  }

  public void setLatitude(Double latitude) {

    this.latitude = latitude;
  }

  public Double getLongitude() {

    return longitude;
  }

  public void setLongitude(Double longitude) {

    this.longitude = longitude;
  }

  public String getServiceUrl() {

    return serviceUrl;
  }

  public void setServiceUrl(String serviceUrl) {

    this.serviceUrl = serviceUrl;
  }
  
  public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getArea() {
		return area;
	}
	
	public void setArea(String area) {
		this.area = area;
	}


}
