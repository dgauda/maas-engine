package com.cap.maas.registrationmanagement.service.api.rest;

import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.web.bind.annotation.PathVariable;

import com.cap.maas.registrationmanagement.logic.api.Registrationmanagement;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationCto;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationEto;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * The service interface for REST calls in order to execute the logic of component {@link Registrationmanagement}.
 */
@Path("/registrationmanagement/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface RegistrationmanagementRestService {

  /**
   * Delegates to {@link Registrationmanagement#findRegistration}.
   *
   * @param id the ID of the {@link RegistrationEto}
   * @return the {@link RegistrationEto}
   */
  @GET
  @Path("/registration/{id}/")
  public RegistrationEto getRegistration(@PathParam("id") long id);

  /**
   * Delegates to {@link Registrationmanagement#saveRegistration}.
   *
   * @param registration the {@link RegistrationEto} to be saved
   * @return the recently created {@link RegistrationEto}
   */
  @POST
  @Path("/registration/")
  public RegistrationEto saveRegistration(RegistrationEto registration);

  /**
   * Delegates to {@link Registrationmanagement#deleteRegistration}.
   *
   * @param id ID of the {@link RegistrationEto} to be deleted
   */
  @DELETE
  @Path("/registration/{id}/")
  public void deleteRegistration(@PathParam("id") long id);

  /**
   * Delegates to {@link Registrationmanagement#findRegistrationEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding registrations.
   * @return the {@link PaginatedListTo list} of matching {@link RegistrationEto}s.
   */
  @Path("/registration/search")
  @POST
  public PaginatedListTo<RegistrationEto> findRegistrationsByPost(RegistrationSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Registrationmanagement#findRegistrationCto}.
   *
   * @param id the ID of the {@link RegistrationCto}
   * @return the {@link RegistrationCto}
   */
  @GET
  @Path("/registration/cto/{id}/")
  public RegistrationCto getRegistrationCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Registrationmanagement#findRegistrationCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding registrations.
   * @return the {@link PaginatedListTo list} of matching {@link RegistrationCto}s.
   */
  @Path("/registration/cto/search")
  @POST
  public PaginatedListTo<RegistrationCto> findRegistrationCtosByPost(RegistrationSearchCriteriaTo searchCriteriaTo);

  @GET
  @Path("/registration/showservices/{id}/{lat}/{lng}/")
  public List<RegistrationEto> showRegisteredServices(@PathParam("id") long id, @PathParam("lat") double latitude,
      @PathParam("lng") double longitude);
  
  @GET
  @Path("/registration/getallservices/{id}/{country}/{city}/{area}/")
  public List<RegistrationEto> getAllRegisteredServices(@PathParam("id") long typeId, @PathParam("country") String country, 
		  @PathParam("city") String city, @PathParam("area") String area);
  
  @GET
  @Path("/registration/getallcountries/{id}/")
  public Set<String> getAllCountries(@PathParam("id") long typeId);
  
  @GET
  @Path("/registration/getallcities/{id}/{country}/")
  public Set<String> getAllCities(@PathParam("id") long typeId,@PathParam("country") String country);
  
  @GET
  @Path("/registration/getallareas/{id}/{country}/{city}/")
  public Set<String> getAllAreas(@PathParam("id") long typeId, @PathParam("country") String country, @PathParam("city") String city);  

}
