package com.cap.maas.registrationmanagement.service.impl.rest;

import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.cap.maas.registrationmanagement.logic.api.Registrationmanagement;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationCto;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationEto;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationSearchCriteriaTo;
import com.cap.maas.registrationmanagement.service.api.rest.RegistrationmanagementRestService;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * The service implementation for REST calls in order to execute the logic of component {@link Registrationmanagement}.
 */
@Named("RegistrationmanagementRestService")
public class RegistrationmanagementRestServiceImpl implements RegistrationmanagementRestService {

  @Inject
  private Registrationmanagement registrationmanagement;

  @Override
  public RegistrationEto getRegistration(long id) {

    return this.registrationmanagement.findRegistration(id);
  }

  @Override
  public RegistrationEto saveRegistration(RegistrationEto registration) {

    return this.registrationmanagement.saveRegistration(registration);
  }

  @Override
  public void deleteRegistration(long id) {

    this.registrationmanagement.deleteRegistration(id);
  }

  @Override
  public PaginatedListTo<RegistrationEto> findRegistrationsByPost(RegistrationSearchCriteriaTo searchCriteriaTo) {

    return this.registrationmanagement.findRegistrationEtos(searchCriteriaTo);
  }

  @Override
  public RegistrationCto getRegistrationCto(long id) {

    return this.registrationmanagement.findRegistrationCto(id);
  }

  @Override
  public PaginatedListTo<RegistrationCto> findRegistrationCtosByPost(RegistrationSearchCriteriaTo searchCriteriaTo) {

    return this.registrationmanagement.findRegistrationCtos(searchCriteriaTo);
  }

  @Override
  public List<RegistrationEto> showRegisteredServices(long id, double latitude, double longitude) {

    return this.registrationmanagement.showRegisteredServices(id, latitude, longitude);
  }
  
  public List<RegistrationEto> getAllRegisteredServices(long typeId, String country, String city,
		  String area){
	  return this.registrationmanagement.getAllRegisteredServices(typeId, country, city, area);
  }
  
  @Override
  public Set<String> getAllCountries(long typeId) {
	  return this.registrationmanagement.getAllCountries(typeId);
  }
  
  @Override
  public Set<String> getAllCities(long typeId, String country){
	  return this.registrationmanagement.getAllCities(typeId, country);
  }
  
  @Override
  public Set<String> getAllAreas(long typeId, String country, String city){
	  return this.registrationmanagement.getAllAreas(typeId, country, city);
  }

}
