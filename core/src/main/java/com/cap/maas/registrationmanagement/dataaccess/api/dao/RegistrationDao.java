package com.cap.maas.registrationmanagement.dataaccess.api.dao;

import com.cap.maas.general.dataaccess.api.dao.ApplicationDao;
import com.cap.maas.registrationmanagement.dataaccess.api.RegistrationEntity;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for Registration entities
 */
public interface RegistrationDao extends ApplicationDao<RegistrationEntity> {

  /**
   * Finds the {@link RegistrationEntity registrations} matching the given {@link RegistrationSearchCriteriaTo}.
   *
   * @param criteria is the {@link RegistrationSearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link RegistrationEntity} objects.
   */
  PaginatedListTo<RegistrationEntity> findRegistrations(RegistrationSearchCriteriaTo criteria);
}
