package com.cap.maas.registrationmanagement.logic.api.to;

import com.cap.maas.general.common.api.to.AbstractEto;
import com.cap.maas.registrationmanagement.common.api.Registration;

/**
 * Entity transport object of Registration
 */
public class RegistrationEto extends AbstractEto implements Registration {

	private static final long serialVersionUID = 1L;

	private String name;

	private String desc;

	private Long typeId;

	private Double latitude;

	private Double longitude;

	private String serviceUrl;

    private String country;
  
    private String city;
  
    private String area;

	private boolean available;

	@Override
	public String getName() {

		return this.name;
	}

	@Override
	public void setName(String name) {

		this.name = name;
	}

	/**
	 * @return desc
	 */
	public String getDesc() {

		return this.desc;
	}

	/**
	 * @param desc
	 *            new value of {@link #getdesc}.
	 */
	public void setDesc(String desc) {

		this.desc = desc;
	}

	@Override
	public Long getTypeId() {

		return this.typeId;
	}

	@Override
	public void setTypeId(Long typeId) {

		this.typeId = typeId;
	}

	@Override
	public Double getLatitude() {

		return this.latitude;
	}

	@Override
	public void setLatitude(Double latitude) {

		this.latitude = latitude;
	}

	@Override
	public Double getLongitude() {

		return this.longitude;
	}

	@Override
	public void setLongitude(Double longitude) {

		this.longitude = longitude;
	}

	@Override
	public String getServiceUrl() {

		return this.serviceUrl;
	}

	@Override
	public void setServiceUrl(String serviceUrl) {

		this.serviceUrl = serviceUrl;
	}


	@Override
	public String getCountry() {
		return country;
	}

	@Override
	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String getCity() {
		return city;
	}

	@Override
	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String getArea() {
		return area;
	}

	@Override
	public void setArea(String area) {
		this.area = area;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.country == null) ? 0 : this.country.hashCode());
		result = prime * result + ((this.city == null) ? 0 : this.city.hashCode());
		result = prime * result + ((this.area == null) ? 0 : this.area.hashCode());
		result = prime * result + ((this.desc == null) ? 0 : this.desc.hashCode());
		result = prime * result + ((this.latitude == null) ? 0 : this.latitude.hashCode());
		result = prime * result + ((this.longitude == null) ? 0 : this.longitude.hashCode());
		result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
		result = prime * result + ((this.serviceUrl == null) ? 0 : this.serviceUrl.hashCode());
		result = prime * result + ((this.typeId == null) ? 0 : this.typeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistrationEto other = (RegistrationEto) obj;
		if (this.country == null) {
			if (other.country != null)
				return false;
		} else if (!this.country.equals(other.country))
			return false;
		if (this.city == null) {
			if (other.city != null)
				return false;
		} else if (!this.city.equals(other.city))
			return false;
		if (this.area == null) {
			if (other.area != null)
				return false;
		} else if (!this.area.equals(other.area))
			return false;		
		if (this.desc == null) {
			if (other.desc != null)
				return false;
		} else if (!this.desc.equals(other.desc))
			return false;
		if (this.latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!this.latitude.equals(other.latitude))
			return false;
		if (this.longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!this.longitude.equals(other.longitude))
			return false;
		if (this.name == null) {
			if (other.name != null)
				return false;
		} else if (!this.name.equals(other.name))
			return false;
		if (this.serviceUrl == null) {
			if (other.serviceUrl != null)
				return false;
		} else if (!this.serviceUrl.equals(other.serviceUrl))
			return false;
		if (this.typeId == null) {
			if (other.typeId != null)
				return false;
		} else if (!this.typeId.equals(other.typeId))
			return false;
		return true;
	}
}
