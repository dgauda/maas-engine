package com.cap.maas.registrationmanagement.logic.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.cap.maas.general.common.api.constants.ApplicationConstants;
import com.cap.maas.general.common.util.MaasEngineUtil;
import com.cap.maas.general.logic.base.AbstractComponentFacade;
import com.cap.maas.itinerarymanagement.dataaccess.api.ActivityTypeEntity;
import com.cap.maas.itinerarymanagement.dataaccess.api.dao.ActivityTypeDao;
import com.cap.maas.itinerarymanagement.logic.api.Itinerarymanagement;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeEto;
import com.cap.maas.registrationmanagement.dataaccess.api.RegistrationEntity;
import com.cap.maas.registrationmanagement.dataaccess.api.dao.RegistrationDao;
import com.cap.maas.registrationmanagement.logic.api.Registrationmanagement;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationCto;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationEto;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;
import io.oasp.module.jpa.common.api.to.PaginationResultTo;

/**
 * Implementation of component interface of registrationmanagement
 */
@Named
@Transactional
public class RegistrationmanagementImpl extends AbstractComponentFacade implements Registrationmanagement {

	/**
	 * Logger instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(RegistrationmanagementImpl.class);

	/**
	 * @see #getRegistrationDao()
	 */
	@Inject
	private RegistrationDao registrationDao;

	/**
	 * @see #getServiceTypeDao()
	 */
	@Inject
	private ActivityTypeDao activityTypeDao;

	@Inject
	private Itinerarymanagement itinerarymanagement;

	/**
	 * The constructor.
	 */
	public RegistrationmanagementImpl() {

		super();
	}

	@Override
	public RegistrationEto findRegistration(Long id) {

		LOG.debug("Get Registration with id {} from database.", id);
		return getBeanMapper().map(getRegistrationDao().findOne(id), RegistrationEto.class);
	}

	@Override
	public PaginatedListTo<RegistrationEto> findRegistrationEtos(RegistrationSearchCriteriaTo criteria) {

		criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
		PaginatedListTo<RegistrationEntity> registrations = getRegistrationDao().findRegistrations(criteria);
		return mapPaginatedEntityList(registrations, RegistrationEto.class);
	}

	@Override
	public boolean deleteRegistration(Long registrationId) {

		RegistrationEntity registration = getRegistrationDao().find(registrationId);
		getRegistrationDao().delete(registration);
		LOG.debug("The registration with id '{}' has been deleted.", registrationId);
		return true;
	}

	@Override
	public RegistrationEto saveRegistration(RegistrationEto registration) {

		Objects.requireNonNull(registration, "registration");
		RegistrationEntity registrationEntity = getBeanMapper().map(registration, RegistrationEntity.class);

		ActivityTypeEntity serviceType = this.activityTypeDao.findOne(registrationEntity.getTypeId());

		registrationEntity.setType(serviceType);

		// initialize, validate registrationEntity here if necessary
		RegistrationEntity resultEntity = getRegistrationDao().save(registrationEntity);

		LOG.debug("Registration with id '{}' has been created.", resultEntity.getId());

		return getBeanMapper().map(resultEntity, RegistrationEto.class);
	}

	/**
	 * Returns the field 'registrationDao'.
	 *
	 * @return the {@link RegistrationDao} instance.
	 */
	public RegistrationDao getRegistrationDao() {

		return this.registrationDao;
	}

	@Override
	public RegistrationCto findRegistrationCto(Long id) {

		LOG.debug("Get RegistrationCto with id {} from database.", id);
		RegistrationEntity entity = getRegistrationDao().findOne(id);
		RegistrationCto cto = new RegistrationCto();
		cto.setRegistration(getBeanMapper().map(entity, RegistrationEto.class));
		cto.setType(getBeanMapper().map(entity.getType(), ActivityTypeEto.class));

		return cto;
	}

	@Override
	public PaginatedListTo<RegistrationCto> findRegistrationCtos(RegistrationSearchCriteriaTo criteria) {

		criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
		PaginatedListTo<RegistrationEntity> registrations = getRegistrationDao().findRegistrations(criteria);
		List<RegistrationCto> ctos = new ArrayList<>();
		for (RegistrationEntity entity : registrations.getResult()) {
			RegistrationCto cto = new RegistrationCto();
			cto.setRegistration(getBeanMapper().map(entity, RegistrationEto.class));
			cto.setType(getBeanMapper().map(entity.getType(), ActivityTypeEto.class));
			ctos.add(cto);

		}
		PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
		PaginatedListTo<RegistrationCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
		return pagListTo;
	}

	/**
	 * Returns the field 'serviceTypeDao'.
	 *
	 * @return the {@link ServiceTypeDao} instance.
	 */
	public ActivityTypeDao getActivityTypeDao() {

		return this.activityTypeDao;
	}

	public List<RegistrationEto> showRegisteredServices(Long id, Double userLat, Double userLong) {

		LOG.info(" Inside showRegisteredServices '{}' '{}' '{}' ", id, userLat, userLong);

		List<RegistrationEto> returnList = new ArrayList<RegistrationEto>();

		// invoke registration table with actTypeid , matching lat and long

		RegistrationSearchCriteriaTo regCriteria = new RegistrationSearchCriteriaTo();
		regCriteria.setTypeId(id);

		List<RegistrationEto> reglist = findRegistrationEtos(regCriteria).getResult();



		for (RegistrationEto regEto : reglist) {
			Double serviceLat = regEto.getLatitude();
			Double serviceLong = regEto.getLongitude();

			// TODO: call the simulator to get the status : available/NotAvailable

			LOG.info(" User Lat and Long '{}' '{}' ", userLat, userLong);
			LOG.info(" Service Lat and Long '{}' '{}' ", serviceLat, serviceLong);

			double distance = distance(userLat, serviceLat, userLong, serviceLong, 0, 0);

			final double distanceInkms = distance / 1000;

			LOG.info(" distanceInkms '{}' km ", distanceInkms);

			if (distanceInkms < 5) {
				
				String response = MaasEngineUtil.getCall(regEto.getServiceUrl() + regEto.getTypeId());
				JSONObject json = MaasEngineUtil.getJSONObject(response);
				int availableNoOfSlots = (Integer) MaasEngineUtil.getJsonDataKeyValue(json, ApplicationConstants.AVAILABLENOOFSLOTS );
				if (availableNoOfSlots > 0)
					regEto.setAvailable(true); 

				returnList.add(regEto);
			}
		}

		return returnList;
	}
	
	
	public Set<String> getAllCountries(long typeId) {
		
		RegistrationSearchCriteriaTo criteria = new RegistrationSearchCriteriaTo();
		criteria.setTypeId(typeId);

		PaginatedListTo<RegistrationEntity> registrations = getRegistrationDao().findRegistrations(criteria);
		
		Set<String> countries = new HashSet<>();
		for (RegistrationEntity entity : registrations.getResult()) {
			String country = entity.getCountry();
			
			countries.add(country);
		}

		return countries;
	}
	  
	public Set<String> getAllCities(long typeId, String country) {
		RegistrationSearchCriteriaTo criteria = new RegistrationSearchCriteriaTo();
		criteria.setTypeId(typeId);
		criteria.setCountry(country);
		
		
		PaginatedListTo<RegistrationEntity> registrations = getRegistrationDao().findRegistrations(criteria);
		
		Set<String> cities = new HashSet<>();
		for (RegistrationEntity entity : registrations.getResult()) {
			String city = entity.getCity();
			
			cities.add(city);
		}

		return cities;
	}
	  
	public Set<String> getAllAreas(long typeId, String country, String city) {
		RegistrationSearchCriteriaTo criteria = new RegistrationSearchCriteriaTo();
		criteria.setTypeId(typeId);
		criteria.setCountry(country);
		criteria.setCity(city);
		
		
		PaginatedListTo<RegistrationEntity> registrations = getRegistrationDao().findRegistrations(criteria);
		
		Set<String> areas = new HashSet<>();
		for (RegistrationEntity entity : registrations.getResult()) {
			String area = entity.getArea();
			
			areas.add(area);
		}

		return areas;
	}
	
	public List<RegistrationEto> getAllRegisteredServices(long typeId, String country, 
			String city, String area) {
		
		RegistrationSearchCriteriaTo criteria = new RegistrationSearchCriteriaTo();
		criteria.setTypeId(typeId);
		criteria.setCountry(country);
		criteria.setCity(city);
		criteria.setArea(area);		
		
		List<RegistrationEto> reglist = findRegistrationEtos(criteria).getResult();
		
		return reglist;
	}

	/**
	 * Calculate distance between two points in latitude and longitude taking into
	 * account height difference. If you are not interested in height difference
	 * pass 0.0. Uses Haversine method as its base.
	 *
	 * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters el2
	 * End altitude in meters
	 *
	 * @returns Distance in Meters
	 */
	private double distance(double lat1, double lat2, double lon1, double lon2, double el1, double el2) {

		final int R = 6371; // Radius of the earth

		if ((lat1 - lat2 == 0.0) && (lon1 - lon2 == 0.0)) {
			return 0.0;
		}

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters

		double height = el1 - el2;

		distance = Math.pow(distance, 2) + Math.pow(height, 2);

		return Math.sqrt(distance);
	}

}
