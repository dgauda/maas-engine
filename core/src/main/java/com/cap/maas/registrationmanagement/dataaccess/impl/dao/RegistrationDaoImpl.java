package com.cap.maas.registrationmanagement.dataaccess.impl.dao;

import java.util.List;

import javax.inject.Named;

import com.cap.maas.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.cap.maas.registrationmanagement.dataaccess.api.RegistrationEntity;
import com.cap.maas.registrationmanagement.dataaccess.api.dao.RegistrationDao;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationSearchCriteriaTo;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link RegistrationDao}.
 */
@Named
public class RegistrationDaoImpl extends ApplicationDaoImpl<RegistrationEntity> implements RegistrationDao {

  /**
   * The constructor.
   */
  public RegistrationDaoImpl() {

    super();
  }

  @Override
  public Class<RegistrationEntity> getEntityClass() {

    return RegistrationEntity.class;
  }

  @Override
  public PaginatedListTo<RegistrationEntity> findRegistrations(RegistrationSearchCriteriaTo criteria) {

    RegistrationEntity registration = Alias.alias(RegistrationEntity.class);
    EntityPathBase<RegistrationEntity> alias = Alias.$(registration);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    String name = criteria.getName();
    if (name != null) {
      query.where(Alias.$(registration.getName()).eq(name));
    }
    Long type = criteria.getTypeId();
    if (type != null) {
      if (registration.getType() != null) {
        query.where(Alias.$(registration.getType().getId()).eq(type));
      }
    }
    Double latitude = criteria.getLatitude();
    if (latitude != null) {
      query.where(Alias.$(registration.getLatitude()).eq(latitude));
    }
    Double longitude = criteria.getLongitude();
    if (longitude != null) {
      query.where(Alias.$(registration.getLongitude()).eq(longitude));
    }
    String serviceUrl = criteria.getServiceUrl();
    if (serviceUrl != null) {
      query.where(Alias.$(registration.getServiceUrl()).eq(serviceUrl));
    }
    String country = criteria.getCountry();
    if (country != null) {
      query.where(Alias.$(registration.getCountry()).eq(country));
    }
    String city = criteria.getCity();
    if (city != null) {
      query.where(Alias.$(registration.getCity()).eq(city));
    }
    String area = criteria.getArea();
    if (area != null) {
      query.where(Alias.$(registration.getArea()).eq(area));
    }
    addOrderBy(query, alias, registration, criteria.getSort());

    return findPaginated(criteria, query, alias);
  }

  private void addOrderBy(JPAQuery query, EntityPathBase<RegistrationEntity> alias, RegistrationEntity registration,
      List<OrderByTo> sort) {

    if (sort != null && !sort.isEmpty()) {
      for (OrderByTo orderEntry : sort) {
        switch (orderEntry.getName()) {
          case "name":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(registration.getName()).asc());
            } else {
              query.orderBy(Alias.$(registration.getName()).desc());
            }
            break;
          case "type":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(registration.getType().getId()).asc());
            } else {
              query.orderBy(Alias.$(registration.getType().getId()).desc());
            }
            break;
          case "latitude":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(registration.getLatitude()).asc());
            } else {
              query.orderBy(Alias.$(registration.getLatitude()).desc());
            }
            break;
          case "longitude":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(registration.getLongitude()).asc());
            } else {
              query.orderBy(Alias.$(registration.getLongitude()).desc());
            }
            break;
          case "serviceUrl":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(registration.getServiceUrl()).asc());
            } else {
              query.orderBy(Alias.$(registration.getServiceUrl()).desc());
            }
            break;
        case "country":
	        if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
	          query.orderBy(Alias.$(registration.getCountry()).asc());
	        } else {
	          query.orderBy(Alias.$(registration.getCountry()).desc());
	        }
	        break;
          case "city":
	        if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
	          query.orderBy(Alias.$(registration.getCity()).asc());
	        } else {
	          query.orderBy(Alias.$(registration.getCity()).desc());
	        }
	        break;
          case "area":
	        if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
	          query.orderBy(Alias.$(registration.getArea()).asc());
	        } else {
	          query.orderBy(Alias.$(registration.getArea()).desc());
	        }
	        break;	        
        }
      }
    }
  }

}