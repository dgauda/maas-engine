package com.cap.maas.registrationmanagement.common.api;

import com.cap.maas.general.common.api.ApplicationEntity;

public interface Registration extends ApplicationEntity {

  public String getName();

  public void setName(String name);

  public Long getTypeId();

  public void setTypeId(Long typeId);

  public Double getLatitude();

  public void setLatitude(Double latitude);

  public Double getLongitude();

  public void setLongitude(Double longitude);

  public String getServiceUrl();

  public void setServiceUrl(String serviceUrl);

  public String getCountry();

  public void setCountry(String country);
	
  public String getCity();
	
  public void setCity(String city);
	
  public String getArea();
	
  public void setArea(String area);

}
