package com.cap.maas.registrationmanagement.logic.api.to;

import com.cap.maas.general.common.api.to.AbstractCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeEto;

/**
 * Composite transport object of Registration
 */
public class RegistrationCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private RegistrationEto registration;

  private ActivityTypeEto type;

  public RegistrationEto getRegistration() {

    return this.registration;
  }

  public void setRegistration(RegistrationEto registration) {

    this.registration = registration;
  }

  public ActivityTypeEto getType() {

    return this.type;
  }

  public void setType(ActivityTypeEto type) {

    this.type = type;
  }

}
