package com.cap.maas.general.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cap.maas.general.common.api.constants.ApplicationConstants;
import com.cap.maas.registrationmanagement.logic.impl.RegistrationmanagementImpl;

public class MaasEngineUtil {

	/**
	 * Logger instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(RegistrationmanagementImpl.class);

	public static String getCall(String url) {

		HttpClient httpClient = HttpClientBuilder.create().build();

		HttpResponse response = null;
		HttpGet httpGet = null;

		try {

			LOG.info("ServiceUrl  '{}'", url);

			httpGet = new HttpGet(url);
			response = httpClient.execute(httpGet);

			return jsonResponseData(response);

		} catch (ClientProtocolException e) { // suppressing exception, to be reviewed with devon fwk
			LOG.error(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
		} finally {
			httpClient.getConnectionManager().shutdown();
			LOG.info("response '{}'", response);
		}

		return null;
	}

	public static String postCall(String url, JSONObject data) {

		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost httpPost = new HttpPost(url);
		HttpResponse response = null;

		try {

			httpPost.setEntity(new StringEntity(data.toString()));
			httpPost.setHeader(ApplicationConstants.HEADER_ACCEPT, ApplicationConstants.APPLICATION_JSON);
			httpPost.setHeader(ApplicationConstants.HEADER_CONTENT_TYPE, ApplicationConstants.APPLICATION_JSON);

			response = httpClient.execute(httpPost);

			return jsonResponseData(response);

		} catch (ClientProtocolException e) { // suppressing exception, to be reviewed with devon fwk
			LOG.error(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
		} finally {
			httpClient.getConnectionManager().shutdown();
			LOG.info("response '{}'", response);
		}

		return null;
	}

	public static JSONObject getJSONObject(String jsonString) {

		try {
			return new JSONObject(jsonString.toString());
		} catch (JSONException e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
		}

		return null;
	}

	public static Object getJsonDataKeyValue(JSONObject json, String keyName) {

		try {
			LOG.info("key '{}' and value '{}'", keyName, json.get(keyName));
			return json.get(keyName);

		} catch (Exception e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	private static String jsonResponseData(HttpResponse response) {

		StringBuilder jsonResponse = new StringBuilder();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
			String output;

			while ((output = br.readLine()) != null) {
				jsonResponse.append(output);
			}
		} catch (IOException e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (Exception e2) {
			}
		}

		return jsonResponse.toString();

	}
}
