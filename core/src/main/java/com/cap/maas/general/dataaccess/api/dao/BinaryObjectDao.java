package com.cap.maas.general.dataaccess.api.dao;

import com.cap.maas.general.dataaccess.api.BinaryObjectEntity;

/**
 * {@link ApplicationDao Data Access Object} for {@link BinaryObjectEntity} entity.
 */
public interface BinaryObjectDao extends ApplicationDao<BinaryObjectEntity> {

}
