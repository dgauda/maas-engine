package com.cap.maas.general.common.api.constants;

public interface ApplicationConstants {

	Long ACTIVITYSTATUS_PENDING = 1L;
	Long ACTIVITYSTATUS_COMPLETE = 2L;
	
	String HEADER_ACCEPT = "Accept";
	String APPLICATION_JSON = "application/json";
	String HEADER_CONTENT_TYPE = "Content-type";

	String AVAILABLENOOFSLOTS = "availableNoOfSlots";
	
}
