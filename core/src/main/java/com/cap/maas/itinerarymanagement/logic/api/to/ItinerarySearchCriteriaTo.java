package com.cap.maas.itinerarymanagement.logic.api.to;

import java.sql.Timestamp;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria} {@link net.sf.mmm.util.transferobject.api.TransferObject TO}
 * used to find {@link com.cap.maas.itinerarymanagement.common.api.Itinerary}s.
 */
public class ItinerarySearchCriteriaTo extends SearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private String name;

  private String desc;

  private Timestamp dateAndTime;

  private Timestamp creationDate;

  private Long customerId;

  private Long statusId;

  /**
   * The constructor.
   */
  public ItinerarySearchCriteriaTo() {

    super();
  }

  public String getName() {

    return this.name;
  }

  public void setName(String name) {

    this.name = name;
  }

  public String getDesc() {

    return this.desc;
  }

  public void setDesc(String desc) {

    this.desc = desc;
  }

  public Timestamp getDateAndTime() {

    return this.dateAndTime;
  }

  public void setDateAndTime(Timestamp dateAndTime) {

    this.dateAndTime = dateAndTime;
  }

  public Timestamp getCreationDate() {

    return creationDate;
  }

  public void setCreationDate(Timestamp creationDate) {

    this.creationDate = creationDate;
  }

  public Long getCustomerId() {

    return customerId;
  }

  public void setCustomerId(Long customerId) {

    this.customerId = customerId;
  }

  public Long getStatusId() {

    return statusId;
  }

  public void setStatusId(Long statusId) {

    this.statusId = statusId;
  }

}
