package com.cap.maas.itinerarymanagement.dataaccess.api.dao;

import com.cap.maas.general.dataaccess.api.dao.ApplicationDao;
import com.cap.maas.itinerarymanagement.dataaccess.api.ActivityTypeEntity;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for ActivityType entities
 */
public interface ActivityTypeDao extends ApplicationDao<ActivityTypeEntity> {

  /**
   * Finds the {@link ActivityTypeEntity activitytypes} matching the given {@link ActivityTypeSearchCriteriaTo}.
   *
   * @param criteria is the {@link ActivityTypeSearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link ActivityTypeEntity} objects.
   */
  PaginatedListTo<ActivityTypeEntity> findActivityTypes(ActivityTypeSearchCriteriaTo criteria);
}
