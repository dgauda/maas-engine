package com.cap.maas.itinerarymanagement.dataaccess.impl.dao;

import java.util.List;

import javax.inject.Named;

import com.cap.maas.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.cap.maas.itinerarymanagement.dataaccess.api.ItineraryStatusEntity;
import com.cap.maas.itinerarymanagement.dataaccess.api.dao.ItineraryStatusDao;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryStatusSearchCriteriaTo;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link ItineraryStatusDao}.
 */
@Named
public class ItineraryStatusDaoImpl extends ApplicationDaoImpl<ItineraryStatusEntity> implements ItineraryStatusDao {

  /**
   * The constructor.
   */
  public ItineraryStatusDaoImpl() {

    super();
  }

  @Override
  public Class<ItineraryStatusEntity> getEntityClass() {

    return ItineraryStatusEntity.class;
  }

  @Override
  public PaginatedListTo<ItineraryStatusEntity> findItineraryStatuss(ItineraryStatusSearchCriteriaTo criteria) {

    ItineraryStatusEntity itinerarystatus = Alias.alias(ItineraryStatusEntity.class);
    EntityPathBase<ItineraryStatusEntity> alias = Alias.$(itinerarystatus);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    String name = criteria.getName();
    if (name != null) {
      query.where(Alias.$(itinerarystatus.getName()).eq(name));
    }
    String desc = criteria.getDesc();
    if (desc != null) {
      query.where(Alias.$(itinerarystatus.getDesc()).eq(desc));
    }
    addOrderBy(query, alias, itinerarystatus, criteria.getSort());

    return findPaginated(criteria, query, alias);
  }

  private void addOrderBy(JPAQuery query, EntityPathBase<ItineraryStatusEntity> alias,
      ItineraryStatusEntity itinerarystatus, List<OrderByTo> sort) {

    if (sort != null && !sort.isEmpty()) {
      for (OrderByTo orderEntry : sort) {
        switch (orderEntry.getName()) {
          case "name":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(itinerarystatus.getName()).asc());
            } else {
              query.orderBy(Alias.$(itinerarystatus.getName()).desc());
            }
            break;
          case "desc":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(itinerarystatus.getDesc()).asc());
            } else {
              query.orderBy(Alias.$(itinerarystatus.getDesc()).desc());
            }
            break;
        }
      }
    }
  }

}
