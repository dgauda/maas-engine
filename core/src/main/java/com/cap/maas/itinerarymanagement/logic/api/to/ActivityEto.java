package com.cap.maas.itinerarymanagement.logic.api.to;

import com.cap.maas.general.common.api.to.AbstractEto;
import com.cap.maas.itinerarymanagement.common.api.Activity;

/**
 * Entity transport object of Activity
 */
public class ActivityEto extends AbstractEto implements Activity {

  private static final long serialVersionUID = 1L;

  private String name;

  private String desc;

  private Long registrationId;

  private Long statusId;

  private Long itineraryId;

  private Long priority;

  /**
   * @return priority
   */
  public Long getPriority() {

    return this.priority;
  }

  /**
   * @param priority new value of {@link #getpriority}.
   */
  public void setPriority(Long priority) {

    this.priority = priority;
  }

  @Override
  public String getName() {

    return this.name;
  }

  @Override
  public void setName(String name) {

    this.name = name;
  }

  @Override
  public String getDesc() {

    return this.desc;
  }

  @Override
  public void setDesc(String desc) {

    this.desc = desc;
  }

  @Override
  public Long getRegistrationId() {

    return this.registrationId;
  }

  @Override
  public void setRegistrationId(Long registrationId) {

    this.registrationId = registrationId;
  }

  @Override
  public Long getStatusId() {

    return this.statusId;
  }

  @Override
  public void setStatusId(Long statusId) {

    this.statusId = statusId;
  }

  @Override
  public Long getItineraryId() {

    return this.itineraryId;
  }

  @Override
  public void setItineraryId(Long itineraryId) {

    this.itineraryId = itineraryId;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((this.desc == null) ? 0 : this.desc.hashCode());
    result = prime * result + ((this.itineraryId == null) ? 0 : this.itineraryId.hashCode());
    result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
    result = prime * result + ((this.priority == null) ? 0 : this.priority.hashCode());
    result = prime * result + ((this.statusId == null) ? 0 : this.statusId.hashCode());
    result = prime * result + ((this.registrationId == null) ? 0 : this.registrationId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    ActivityEto other = (ActivityEto) obj;
    if (this.desc == null) {
      if (other.desc != null)
        return false;
    } else if (!this.desc.equals(other.desc))
      return false;
    if (this.itineraryId == null) {
      if (other.itineraryId != null)
        return false;
    } else if (!this.itineraryId.equals(other.itineraryId))
      return false;
    if (this.name == null) {
      if (other.name != null)
        return false;
    } else if (!this.name.equals(other.name))
      return false;
    if (this.priority == null) {
      if (other.priority != null)
        return false;
    } else if (!this.priority.equals(other.priority))
      return false;
    if (this.statusId == null) {
      if (other.statusId != null)
        return false;
    } else if (!this.statusId.equals(other.statusId))
      return false;
    if (this.registrationId == null) {
      if (other.registrationId != null)
        return false;
    } else if (!this.registrationId.equals(other.registrationId))
      return false;
    return true;
  }

}
