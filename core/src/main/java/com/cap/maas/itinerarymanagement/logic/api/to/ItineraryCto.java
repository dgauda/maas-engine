package com.cap.maas.itinerarymanagement.logic.api.to;

import java.util.List;

import com.cap.maas.general.common.api.to.AbstractCto;

/**
 * Composite transport object of Itinerary
 */
public class ItineraryCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private ItineraryEto itinerary;

  private List<ActivityEto> activities;

  private CustomerEto customer;

  private ItineraryStatusEto status;

  public ItineraryEto getItinerary() {

    return this.itinerary;
  }

  public void setItinerary(ItineraryEto itinerary) {

    this.itinerary = itinerary;
  }

  public List<ActivityEto> getActivities() {

    return this.activities;
  }

  public void setActivities(List<ActivityEto> activities) {

    this.activities = activities;
  }

  public CustomerEto getCustomer() {

    return customer;
  }

  public void setCustomer(CustomerEto customer) {

    this.customer = customer;
  }

  public ItineraryStatusEto getStatus() {

    return status;
  }

  public void setStatus(ItineraryStatusEto status) {

    this.status = status;
  }

}
