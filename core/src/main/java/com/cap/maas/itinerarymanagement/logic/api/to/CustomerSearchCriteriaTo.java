package com.cap.maas.itinerarymanagement.logic.api.to;

import java.sql.Timestamp;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria} {@link net.sf.mmm.util.transferobject.api.TransferObject TO}
 * used to find {@link com.cap.maas.itinerarymanagement.common.api.Customer}s.
 *
 */
public class CustomerSearchCriteriaTo extends SearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private String name;

  private Integer contactnumber;

  private Boolean isregularcustomer;

  private Timestamp registrationDate;

  /**
   * The constructor.
   */
  public CustomerSearchCriteriaTo() {

    super();
  }

  public String getName() {

    return this.name;
  }

  public void setName(String name) {

    this.name = name;
  }

  public Integer getContactnumber() {

    return this.contactnumber;
  }

  public void setContactnumber(Integer contactnumber) {

    this.contactnumber = contactnumber;
  }

  public Boolean isIsregularcustomer() {

    return this.isregularcustomer;
  }

  public void setIsregularcustomer(Boolean isregularcustomer) {

    this.isregularcustomer = isregularcustomer;
  }

  public Timestamp getRegistrationDate() {

    return this.registrationDate;
  }

  public void setRegistrationDate(Timestamp registrationDate) {

    this.registrationDate = registrationDate;
  }

}
