package com.cap.maas.itinerarymanagement.logic.api.to;

import java.sql.Timestamp;

import com.cap.maas.general.common.api.to.AbstractEto;
import com.cap.maas.itinerarymanagement.common.api.Customer;

/**
 * Entity transport object of Customer
 */
public class CustomerEto extends AbstractEto implements Customer {

  private static final long serialVersionUID = 1L;

  private String name;

  private int contactnumber;

  private boolean isregularcustomer;

  private Timestamp registrationDate;

  @Override
  public String getName() {

    return this.name;
  }

  @Override
  public void setName(String name) {

    this.name = name;
  }

  @Override
  public int getContactnumber() {

    return this.contactnumber;
  }

  @Override
  public void setContactnumber(int contactnumber) {

    this.contactnumber = contactnumber;
  }

  @Override
  public boolean isIsregularcustomer() {

    return this.isregularcustomer;
  }

  @Override
  public void setIsregularcustomer(boolean isregularcustomer) {

    this.isregularcustomer = isregularcustomer;
  }

  @Override
  public Timestamp getRegistrationDate() {

    return this.registrationDate;
  }

  @Override
  public void setRegistrationDate(Timestamp registrationDate) {

    this.registrationDate = registrationDate;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
    result = prime * result + ((Integer) this.contactnumber).hashCode();
    result = prime * result + ((Boolean) this.isregularcustomer).hashCode();
    result = prime * result + ((this.registrationDate == null) ? 0 : this.registrationDate.hashCode());

    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    CustomerEto other = (CustomerEto) obj;
    if (this.name == null) {
      if (other.name != null) {
        return false;
      }
    } else if (!this.name.equals(other.name)) {
      return false;
    }
    if (this.contactnumber != other.contactnumber) {
      return false;
    }
    if (this.isregularcustomer != other.isregularcustomer) {
      return false;
    }
    if (this.registrationDate == null) {
      if (other.registrationDate != null) {
        return false;
      }
    } else if (!this.registrationDate.equals(other.registrationDate)) {
      return false;
    }

    return true;
  }
}
