package com.cap.maas.itinerarymanagement.logic.api.to;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria} {@link net.sf.mmm.util.transferobject.api.TransferObject TO}
 * used to find {@link com.cap.maas.itinerarymanagement.common.api.ItineraryStatus}s.
 */
public class ItineraryStatusSearchCriteriaTo extends SearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private String name;

  private String desc;

  /**
   * The constructor.
   */
  public ItineraryStatusSearchCriteriaTo() {

    super();
  }

  public String getName() {

    return name;
  }

  public void setName(String name) {

    this.name = name;
  }

  public String getDesc() {

    return desc;
  }

  public void setDesc(String desc) {

    this.desc = desc;
  }

}
