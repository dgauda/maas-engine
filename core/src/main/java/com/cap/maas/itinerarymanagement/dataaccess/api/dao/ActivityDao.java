package com.cap.maas.itinerarymanagement.dataaccess.api.dao;

import com.cap.maas.general.dataaccess.api.dao.ApplicationDao;
import com.cap.maas.itinerarymanagement.dataaccess.api.ActivityEntity;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivitySearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for Activity entities
 */
public interface ActivityDao extends ApplicationDao<ActivityEntity> {

  /**
   * Finds the {@link ActivityEntity activitys} matching the given {@link ActivitySearchCriteriaTo}.
   *
   * @param criteria is the {@link ActivitySearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link ActivityEntity} objects.
   */
  PaginatedListTo<ActivityEntity> findActivitys(ActivitySearchCriteriaTo criteria);
}
