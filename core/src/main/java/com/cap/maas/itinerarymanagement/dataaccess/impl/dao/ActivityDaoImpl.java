package com.cap.maas.itinerarymanagement.dataaccess.impl.dao;

import java.util.List;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cap.maas.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.cap.maas.itinerarymanagement.dataaccess.api.ActivityEntity;
import com.cap.maas.itinerarymanagement.dataaccess.api.dao.ActivityDao;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivitySearchCriteriaTo;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link ActivityDao}.
 */
@Named
public class ActivityDaoImpl extends ApplicationDaoImpl<ActivityEntity> implements ActivityDao {

  private static final Logger LOG = LoggerFactory.getLogger(ActivityDaoImpl.class);

  /**
   * The constructor.
   */
  public ActivityDaoImpl() {

    super();
  }

  @Override
  public Class<ActivityEntity> getEntityClass() {

    return ActivityEntity.class;
  }

  @Override
  public PaginatedListTo<ActivityEntity> findActivitys(ActivitySearchCriteriaTo criteria) {

    ActivityEntity activity = Alias.alias(ActivityEntity.class);
    EntityPathBase<ActivityEntity> alias = Alias.$(activity);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    String name = criteria.getName();
    if (name != null) {
      query.where(Alias.$(activity.getName()).eq(name));
    }
    String desc = criteria.getDesc();
    if (desc != null) {
      query.where(Alias.$(activity.getDesc()).eq(desc));
    }
    Long regId = criteria.getRegistrationId();
    if (regId != null) {
      if (activity.getRegistrationEntity() != null) {
        query.where(Alias.$(activity.getRegistrationEntity().getId()).eq(regId));
      }
    }
    Long status = criteria.getStatusId();
    if (status != null) {
      if (activity.getStatus() != null) {
        query.where(Alias.$(activity.getStatus().getId()).eq(status));
      }
    }

    Long itinerary = criteria.getItineraryId();
    if (itinerary != null) {
      if (activity.getItinerary() != null) {
        query.where(Alias.$(activity.getItinerary().getId()).eq(itinerary));
      }
    }
    Long priority = criteria.getPriority();
    if (priority != null) {
      if (activity.getPriority() != null) {
        query.where(Alias.$(activity.getPriority()).eq(priority));
      }
    }

    addOrderBy(query, alias, activity, criteria.getSort());

    LOG.info("query '{}'", query);

    return findPaginated(criteria, query, alias);
  }

  private void addOrderBy(JPAQuery query, EntityPathBase<ActivityEntity> alias, ActivityEntity activity,
      List<OrderByTo> sort) {

    if (sort != null && !sort.isEmpty()) {
      for (OrderByTo orderEntry : sort) {
        switch (orderEntry.getName()) {
          case "name":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(activity.getName()).asc());
            } else {
              query.orderBy(Alias.$(activity.getName()).desc());
            }
            break;
          case "desc":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(activity.getDesc()).asc());
            } else {
              query.orderBy(Alias.$(activity.getDesc()).desc());
            }
            break;
          case "type":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(activity.getRegistrationEntity().getId()).asc());
            } else {
              query.orderBy(Alias.$(activity.getRegistrationEntity().getId()).desc());
            }
            break;
          case "itinerary":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(activity.getItinerary().getId()).asc());
            } else {
              query.orderBy(Alias.$(activity.getItinerary().getId()).desc());
            }
            break;
          case "priority":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(activity.getPriority()).asc());
            } else {
              query.orderBy(Alias.$(activity.getPriority()).desc());
            }
            break;
        }
      }
    }
  }

}