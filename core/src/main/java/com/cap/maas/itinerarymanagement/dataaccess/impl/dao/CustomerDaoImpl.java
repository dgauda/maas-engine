package com.cap.maas.itinerarymanagement.dataaccess.impl.dao;

import java.sql.Timestamp;
import java.util.List;

import javax.inject.Named;

import com.cap.maas.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.cap.maas.itinerarymanagement.dataaccess.api.CustomerEntity;
import com.cap.maas.itinerarymanagement.dataaccess.api.dao.CustomerDao;
import com.cap.maas.itinerarymanagement.logic.api.to.CustomerSearchCriteriaTo;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link CustomerDao}.
 */
@Named
public class CustomerDaoImpl extends ApplicationDaoImpl<CustomerEntity> implements CustomerDao {

  /**
   * The constructor.
   */
  public CustomerDaoImpl() {

    super();
  }

  @Override
  public Class<CustomerEntity> getEntityClass() {

    return CustomerEntity.class;
  }

  @Override
  public PaginatedListTo<CustomerEntity> findCustomers(CustomerSearchCriteriaTo criteria) {

    CustomerEntity customer = Alias.alias(CustomerEntity.class);
    EntityPathBase<CustomerEntity> alias = Alias.$(customer);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    String name = criteria.getName();
    if (name != null) {
      query.where(Alias.$(customer.getName()).eq(name));
    }
    Integer contactnumber = criteria.getContactnumber();
    if (contactnumber != null) {
      query.where(Alias.$(customer.getContactnumber()).eq(contactnumber));
    }
    Boolean isregularcustomer = criteria.isIsregularcustomer();
    if (isregularcustomer != null) {
      query.where(Alias.$(customer.isIsregularcustomer()).eq(isregularcustomer));
    }
    Timestamp registrationDate = criteria.getRegistrationDate();
    if (registrationDate != null) {
      query.where(Alias.$(customer.getRegistrationDate()).eq(registrationDate));
    }
    addOrderBy(query, alias, customer, criteria.getSort());

    return findPaginated(criteria, query, alias);
  }

  private void addOrderBy(JPAQuery query, EntityPathBase<CustomerEntity> alias, CustomerEntity customer,
      List<OrderByTo> sort) {

    if (sort != null && !sort.isEmpty()) {
      for (OrderByTo orderEntry : sort) {
        switch (orderEntry.getName()) {
          case "name":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(customer.getName()).asc());
            } else {
              query.orderBy(Alias.$(customer.getName()).desc());
            }
            break;
          case "contactnumber":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(customer.getContactnumber()).asc());
            } else {
              query.orderBy(Alias.$(customer.getContactnumber()).desc());
            }
            break;
          case "isregularcustomer":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(customer.isIsregularcustomer()).asc());
            } else {
              query.orderBy(Alias.$(customer.isIsregularcustomer()).desc());
            }
            break;
          case "registrationDate":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(customer.getRegistrationDate()).asc());
            } else {
              query.orderBy(Alias.$(customer.getRegistrationDate()).desc());
            }
            break;
        }
      }
    }
  }

}