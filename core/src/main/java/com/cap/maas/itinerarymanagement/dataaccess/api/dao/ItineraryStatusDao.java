package com.cap.maas.itinerarymanagement.dataaccess.api.dao;

import com.cap.maas.general.dataaccess.api.dao.ApplicationDao;
import com.cap.maas.itinerarymanagement.dataaccess.api.ItineraryStatusEntity;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryStatusSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for ItineraryStatus entities
 */
public interface ItineraryStatusDao extends ApplicationDao<ItineraryStatusEntity> {

  /**
   * Finds the {@link ItineraryStatusEntity itinerarystatuss} matching the given
   * {@link ItineraryStatusSearchCriteriaTo}.
   *
   * @param criteria is the {@link ItineraryStatusSearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link ItineraryStatusEntity} objects.
   */
  PaginatedListTo<ItineraryStatusEntity> findItineraryStatuss(ItineraryStatusSearchCriteriaTo criteria);

}
