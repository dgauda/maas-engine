package com.cap.maas.itinerarymanagement.common.api;

import com.cap.maas.general.common.api.ApplicationEntity;

public interface Activity extends ApplicationEntity {

  public String getName();

  public void setName(String name);

  public String getDesc();

  public void setDesc(String desc);

  public Long getRegistrationId();

  public void setRegistrationId(Long registrationId);

  public Long getStatusId();

  public void setStatusId(Long typeId);

  public Long getItineraryId();

  public void setItineraryId(Long itineraryId);

}
