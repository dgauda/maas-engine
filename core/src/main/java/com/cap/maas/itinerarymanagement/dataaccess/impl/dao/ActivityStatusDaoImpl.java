package com.cap.maas.itinerarymanagement.dataaccess.impl.dao;

import java.util.List;

import javax.inject.Named;

import com.cap.maas.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.cap.maas.itinerarymanagement.dataaccess.api.ActivityStatusEntity;
import com.cap.maas.itinerarymanagement.dataaccess.api.dao.ActivityStatusDao;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityStatusSearchCriteriaTo;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link ActivityStatusDao}.
 */
@Named
public class ActivityStatusDaoImpl extends ApplicationDaoImpl<ActivityStatusEntity> implements ActivityStatusDao {

  /**
   * The constructor.
   */
  public ActivityStatusDaoImpl() {

    super();
  }

  @Override
  public Class<ActivityStatusEntity> getEntityClass() {

    return ActivityStatusEntity.class;
  }

  @Override
  public PaginatedListTo<ActivityStatusEntity> findActivityStatuss(ActivityStatusSearchCriteriaTo criteria) {

    ActivityStatusEntity activitystatus = Alias.alias(ActivityStatusEntity.class);
    EntityPathBase<ActivityStatusEntity> alias = Alias.$(activitystatus);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    String name = criteria.getName();
    if (name != null) {
      query.where(Alias.$(activitystatus.getName()).eq(name));
    }
    String desc = criteria.getDesc();
    if (desc != null) {
      query.where(Alias.$(activitystatus.getDesc()).eq(desc));
    }
    addOrderBy(query, alias, activitystatus, criteria.getSort());

    return findPaginated(criteria, query, alias);
  }

  private void addOrderBy(JPAQuery query, EntityPathBase<ActivityStatusEntity> alias,
      ActivityStatusEntity activitystatus, List<OrderByTo> sort) {

    if (sort != null && !sort.isEmpty()) {
      for (OrderByTo orderEntry : sort) {
        switch (orderEntry.getName()) {
          case "name":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(activitystatus.getName()).asc());
            } else {
              query.orderBy(Alias.$(activitystatus.getName()).desc());
            }
            break;
          case "desc":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(activitystatus.getDesc()).asc());
            } else {
              query.orderBy(Alias.$(activitystatus.getDesc()).desc());
            }
            break;
        }
      }
    }
  }

}
