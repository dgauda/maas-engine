package com.cap.maas.itinerarymanagement.logic.api.to;

import com.cap.maas.general.common.api.to.AbstractCto;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationEto;

/**
 * Composite transport object of Activity
 */
public class ActivityCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private ActivityEto activity;

  private RegistrationEto registrationEto;

  private ActivityStatusEto status;

  private ItineraryEto itinerary;

  public ActivityEto getActivity() {

    return this.activity;
  }

  public void setActivity(ActivityEto activity) {

    this.activity = activity;
  }

  public RegistrationEto getRegistrationEto() {

    return this.registrationEto;
  }

  public void setRegistrationEto(RegistrationEto registrationEto) {

    this.registrationEto = registrationEto;
  }

  public ActivityStatusEto getStatus() {

    return this.status;
  }

  public void setStatus(ActivityStatusEto status) {

    this.status = status;
  }

  public ItineraryEto getItinerary() {

    return this.itinerary;
  }

  public void setItinerary(ItineraryEto itinerary) {

    this.itinerary = itinerary;
  }

}
