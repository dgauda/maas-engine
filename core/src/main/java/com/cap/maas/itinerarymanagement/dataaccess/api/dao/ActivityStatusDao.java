package com.cap.maas.itinerarymanagement.dataaccess.api.dao;

import com.cap.maas.general.dataaccess.api.dao.ApplicationDao;
import com.cap.maas.itinerarymanagement.dataaccess.api.ActivityStatusEntity;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityStatusSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for ActivityStatus entities
 */
public interface ActivityStatusDao extends ApplicationDao<ActivityStatusEntity> {

  /**
   * Finds the {@link ActivityStatusEntity activitystatuss} matching the given {@link ActivityStatusSearchCriteriaTo}.
   *
   * @param criteria is the {@link ActivityStatusSearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link ActivityStatusEntity} objects.
   */
  PaginatedListTo<ActivityStatusEntity> findActivityStatuss(ActivityStatusSearchCriteriaTo criteria);

}
