package com.cap.maas.itinerarymanagement.logic.api.to;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria} {@link net.sf.mmm.util.transferobject.api.TransferObject TO}
 * used to find {@link com.cap.maas.itinerarymanagement.common.api.Activity}s.
 */
public class ActivitySearchCriteriaTo extends SearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private String name;

  private String desc;

  private Long registrationId;

  private Long statusId;

  private Long itineraryId;

  private Long priority;

  /**
   * @return statusId
   */
  public Long getStatusId() {

    return this.statusId;
  }

  /**
   * @param statusId new value of {@link #getstatusId}.
   */
  public void setStatusId(Long statusId) {

    this.statusId = statusId;
  }

  /**
   * @return priority
   */
  public Long getPriority() {

    return this.priority;
  }

  /**
   * @param priority new value of {@link #getpriority}.
   */
  public void setPriority(Long priority) {

    this.priority = priority;
  }

  /**
   * The constructor.
   */
  public ActivitySearchCriteriaTo() {

    super();
  }

  public String getName() {

    return this.name;
  }

  public void setName(String name) {

    this.name = name;
  }

  public String getDesc() {

    return this.desc;
  }

  public void setDesc(String desc) {

    this.desc = desc;
  }

  public Long getRegistrationId() {

    return this.registrationId;
  }

  public void setRegistrationId(Long registrationId) {

    this.registrationId = registrationId;
  }

  public Long getItineraryId() {

    return this.itineraryId;
  }

  public void setItineraryId(Long itineraryId) {

    this.itineraryId = itineraryId;
  }

}
