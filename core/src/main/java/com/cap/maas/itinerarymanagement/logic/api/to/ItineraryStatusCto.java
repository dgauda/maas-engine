package com.cap.maas.itinerarymanagement.logic.api.to;

import com.cap.maas.general.common.api.to.AbstractCto;

/**
 * Composite transport object of ItineraryStatus
 */
public class ItineraryStatusCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private ItineraryStatusEto itineraryStatus;

  public ItineraryStatusEto getItineraryStatus() {

    return itineraryStatus;
  }

  public void setItineraryStatus(ItineraryStatusEto itineraryStatus) {

    this.itineraryStatus = itineraryStatus;
  }

}
