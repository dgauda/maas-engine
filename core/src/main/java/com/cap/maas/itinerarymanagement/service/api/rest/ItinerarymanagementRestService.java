package com.cap.maas.itinerarymanagement.service.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cap.maas.itinerarymanagement.logic.api.Itinerarymanagement;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivitySearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityStatusCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityStatusEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityStatusSearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeSearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.CustomerCto;
import com.cap.maas.itinerarymanagement.logic.api.to.CustomerEto;
import com.cap.maas.itinerarymanagement.logic.api.to.CustomerSearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItinerarySearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryStatusCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryStatusEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryStatusSearchCriteriaTo;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationEto;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * The service interface for REST calls in order to execute the logic of component {@link Itinerarymanagement}.
 */
@Path("/itinerarymanagement/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface ItinerarymanagementRestService {

  /**
   * Delegates to {@link Itinerarymanagement#findItinerary}.
   *
   * @param id the ID of the {@link ItineraryEto}
   * @return the {@link ItineraryEto}
   */
  @GET
  @Path("/itinerary/{id}/")
  public ItineraryEto getItinerary(@PathParam("id") long id);

  @GET
  @Path("/itinerary/")
  public PaginatedListTo<ItineraryEto> getAllItineraries();

  /**
   * Delegates to {@link Itinerarymanagement#saveItinerary}.
   *
   * @param itinerary the {@link ItineraryEto} to be saved
   * @return the recently created {@link ItineraryEto}
   */
  @POST
  @Path("/itinerary/")
  public ItineraryEto saveItinerary(ItineraryEto itinerary);

  /**
   * Delegates to {@link Itinerarymanagement#deleteItinerary}.
   *
   * @param id ID of the {@link ItineraryEto} to be deleted
   */
  @DELETE
  @Path("/itinerary/{id}/")
  public void deleteItinerary(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#findItineraryEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding itinerarys.
   * @return the {@link PaginatedListTo list} of matching {@link ItineraryEto}s.
   */
  @Path("/itinerary/search")
  @POST
  public PaginatedListTo<ItineraryEto> findItinerarysByPost(ItinerarySearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Itinerarymanagement#findItineraryCto}.
   *
   * @param id the ID of the {@link ItineraryCto}
   * @return the {@link ItineraryCto}
   */
  @GET
  @Path("/itinerary/cto/{id}/")
  public ItineraryCto getItineraryCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#saveItinerary}.
   *
   * @param itinerary the {@link ItineraryCto} to be saved
   * @return the recently created {@link ItineraryCto}
   */
  @POST
  @Path("/itinerary/cto/")
  public ItineraryEto saveItinerary(ItineraryCto itinerary);

  /**
   * Delegates to {@link Itinerarymanagement#findItineraryCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding itinerarys.
   * @return the {@link PaginatedListTo list} of matching {@link ItineraryCto}s.
   */
  @Path("/itinerary/cto/search")
  @POST
  public PaginatedListTo<ItineraryCto> findItineraryCtosByPost(ItinerarySearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Itinerarymanagement#findActivity}.
   *
   * @param id the ID of the {@link ActivityEto}
   * @return the {@link ActivityEto}
   */
  @GET
  @Path("/activity/{id}/")
  public ActivityEto getActivity(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#saveActivity}.
   *
   * @param activity the {@link ActivityEto} to be saved
   * @return the recently created {@link ActivityEto}
   */
  @POST
  @Path("/activity/")
  public ActivityEto saveActivity(ActivityEto activity);

  /**
   * Delegates to {@link Itinerarymanagement#deleteActivity}.
   *
   * @param id ID of the {@link ActivityEto} to be deleted
   */
  @DELETE
  @Path("/activity/{id}/")
  public void deleteActivity(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#findActivityEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding activitys.
   * @return the {@link PaginatedListTo list} of matching {@link ActivityEto}s.
   */
  @Path("/activity/search")
  @POST
  public PaginatedListTo<ActivityEto> findActivitysByPost(ActivitySearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Itinerarymanagement#findActivityCto}.
   *
   * @param id the ID of the {@link ActivityCto}
   * @return the {@link ActivityCto}
   */
  @GET
  @Path("/activity/cto/{id}/")
  public ActivityCto getActivityCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#findActivityCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding activitys.
   * @return the {@link PaginatedListTo list} of matching {@link ActivityCto}s.
   */
  @Path("/activity/cto/search")
  @POST
  public PaginatedListTo<ActivityCto> findActivityCtosByPost(ActivitySearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Itinerarymanagement#findActivityType}.
   *
   * @param id the ID of the {@link ActivityTypeEto}
   * @return the {@link ActivityTypeEto}
   */
  @GET
  @Path("/activitytype/{id}/")
  public ActivityTypeEto getActivityType(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#saveActivityType}.
   *
   * @param activitytype the {@link ActivityTypeEto} to be saved
   * @return the recently created {@link ActivityTypeEto}
   */
  @POST
  @Path("/activitytype/")
  public ActivityTypeEto saveActivityType(ActivityTypeEto activitytype);

  /**
   * Delegates to {@link Itinerarymanagement#deleteActivityType}.
   *
   * @param id ID of the {@link ActivityTypeEto} to be deleted
   */
  @DELETE
  @Path("/activitytype/{id}/")
  public void deleteActivityType(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#findActivityTypeEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding activitytypes.
   * @return the {@link PaginatedListTo list} of matching {@link ActivityTypeEto}s.
   */
  @Path("/activitytype/search")
  @POST
  public PaginatedListTo<ActivityTypeEto> findActivityTypesByPost(ActivityTypeSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Itinerarymanagement#findActivityTypeCto}.
   *
   * @param id the ID of the {@link ActivityTypeCto}
   * @return the {@link ActivityTypeCto}
   */
  @GET
  @Path("/activitytype/cto/{id}/")
  public ActivityTypeCto getActivityTypeCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#findActivityTypeCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding activitytypes.
   * @return the {@link PaginatedListTo list} of matching {@link ActivityTypeCto}s.
   */
  @Path("/activitytype/cto/search")
  @POST
  public PaginatedListTo<ActivityTypeCto> findActivityTypeCtosByPost(ActivityTypeSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Itinerarymanagement#findActivityStatus}.
   *
   * @param id the ID of the {@link ActivityStatusEto}
   * @return the {@link ActivityStatusEto}
   */
  @GET
  @Path("/activitystatus/{id}/")
  public ActivityStatusEto getActivityStatus(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#saveActivityStatus}.
   *
   * @param activitystatus the {@link ActivityStatusEto} to be saved
   * @return the recently created {@link ActivityStatusEto}
   */
  @POST
  @Path("/activitystatus/")
  public ActivityStatusEto saveActivityStatus(ActivityStatusEto activitystatus);

  /**
   * Delegates to {@link Itinerarymanagement#deleteActivityStatus}.
   *
   * @param id ID of the {@link ActivityStatusEto} to be deleted
   */
  @DELETE
  @Path("/activitystatus/{id}/")
  public void deleteActivityStatus(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#findActivityStatusEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding activitystatuss.
   * @return the {@link PaginatedListTo list} of matching {@link ActivityStatusEto}s.
   */
  @Path("/activitystatus/search")
  @POST
  public PaginatedListTo<ActivityStatusEto> findActivityStatussByPost(ActivityStatusSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Itinerarymanagement#findActivityStatusCto}.
   *
   * @param id the ID of the {@link ActivityStatusCto}
   * @return the {@link ActivityStatusCto}
   */
  @GET
  @Path("/activitystatus/cto/{id}/")
  public ActivityStatusCto getActivityStatusCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#findActivityStatusCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding activitystatuss.
   * @return the {@link PaginatedListTo list} of matching {@link ActivityStatusCto}s.
   */
  @Path("/activitystatus/cto/search")
  @POST
  public PaginatedListTo<ActivityStatusCto> findActivityStatusCtosByPost(
      ActivityStatusSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Itinerarymanagement#reset}.
   *
   * @param id the ID of the {@link ActivityEto}
   */
  @GET
  @Path("/itinerary/activity/reset/{id}/")
  public void resetActivity(Long id);

  /**
   * Delegates to {@link Itinerarymanagement#book}.
   *
   * @param id the ID of the {@link ActivityEto}
   * @param servId the ID of the {@link RegistrationEto}
   * @return the {@link ActivityEto}
   */
  @GET
  @Path("/itinerary/activity/book/{id}/{servId}/")
  public ActivityEto bookActivity(@PathParam("id") long id, @PathParam("servId") long servId);

  /**
   * Delegates to {@link Itinerarymanagement#findCustomer}.
   *
   * @param id the ID of the {@link CustomerEto}
   * @return the {@link CustomerEto}
   */
  @GET
  @Path("/customer/{id}/")
  public CustomerEto getCustomer(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#saveCustomer}.
   *
   * @param customer the {@link CustomerEto} to be saved
   * @return the recently created {@link CustomerEto}
   */
  @POST
  @Path("/customer/")
  public CustomerEto saveCustomer(CustomerEto customer);

  /**
   * Delegates to {@link Itinerarymanagement#deleteCustomer}.
   *
   * @param id ID of the {@link CustomerEto} to be deleted
   */
  @DELETE
  @Path("/customer/{id}/")
  public void deleteCustomer(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#findCustomerEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding customers.
   * @return the {@link PaginatedListTo list} of matching {@link CustomerEto}s.
   */
  @Path("/customer/search")
  @POST
  public PaginatedListTo<CustomerEto> findCustomersByPost(CustomerSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Itinerarymanagement#findCustomerCto}.
   *
   * @param id the ID of the {@link CustomerCto}
   * @return the {@link CustomerCto}
   */
  @GET
  @Path("/customer/cto/{id}/")
  public CustomerCto getCustomerCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#findCustomerCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding customers.
   * @return the {@link PaginatedListTo list} of matching {@link CustomerCto}s.
   */
  @Path("/customer/cto/search")
  @POST
  public PaginatedListTo<CustomerCto> findCustomerCtosByPost(CustomerSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Itinerarymanagement#findItineraryStatus}.
   *
   * @param id the ID of the {@link ItineraryStatusEto}
   * @return the {@link ItineraryStatusEto}
   */
  @GET
  @Path("/itinerarystatus/{id}/")
  public ItineraryStatusEto getItineraryStatus(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#saveItineraryStatus}.
   *
   * @param itinerarystatus the {@link ItineraryStatusEto} to be saved
   * @return the recently created {@link ItineraryStatusEto}
   */
  @POST
  @Path("/itinerarystatus/")
  public ItineraryStatusEto saveItineraryStatus(ItineraryStatusEto itinerarystatus);

  /**
   * Delegates to {@link Itinerarymanagement#deleteItineraryStatus}.
   *
   * @param id ID of the {@link ItineraryStatusEto} to be deleted
   */
  @DELETE
  @Path("/itinerarystatus/{id}/")
  public void deleteItineraryStatus(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#findItineraryStatusEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding itinerarystatuss.
   * @return the {@link PaginatedListTo list} of matching {@link ItineraryStatusEto}s.
   */
  @Path("/itinerarystatus/search")
  @POST
  public PaginatedListTo<ItineraryStatusEto> findItineraryStatussByPost(
      ItineraryStatusSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Itinerarymanagement#findItineraryStatusCto}.
   *
   * @param id the ID of the {@link ItineraryStatusCto}
   * @return the {@link ItineraryStatusCto}
   */
  @GET
  @Path("/itinerarystatus/cto/{id}/")
  public ItineraryStatusCto getItineraryStatusCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Itinerarymanagement#findItineraryStatusCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding itinerarystatuss.
   * @return the {@link PaginatedListTo list} of matching {@link ItineraryStatusCto}s.
   */
  @Path("/itinerarystatus/cto/search")
  @POST
  public PaginatedListTo<ItineraryStatusCto> findItineraryStatusCtosByPost(
      ItineraryStatusSearchCriteriaTo searchCriteriaTo);

}
