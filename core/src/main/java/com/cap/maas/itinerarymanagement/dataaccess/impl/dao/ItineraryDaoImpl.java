package com.cap.maas.itinerarymanagement.dataaccess.impl.dao;

import java.sql.Timestamp;
import java.util.List;

import javax.inject.Named;

import com.cap.maas.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.cap.maas.itinerarymanagement.dataaccess.api.ItineraryEntity;
import com.cap.maas.itinerarymanagement.dataaccess.api.dao.ItineraryDao;
import com.cap.maas.itinerarymanagement.logic.api.to.ItinerarySearchCriteriaTo;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link ItineraryDao}.
 */
@Named
public class ItineraryDaoImpl extends ApplicationDaoImpl<ItineraryEntity> implements ItineraryDao {

  /**
   * The constructor.
   */
  public ItineraryDaoImpl() {

    super();
  }

  @Override
  public Class<ItineraryEntity> getEntityClass() {

    return ItineraryEntity.class;
  }

  @Override
  public PaginatedListTo<ItineraryEntity> findItinerarys(ItinerarySearchCriteriaTo criteria) {

    ItineraryEntity itinerary = Alias.alias(ItineraryEntity.class);
    EntityPathBase<ItineraryEntity> alias = Alias.$(itinerary);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    String name = criteria.getName();
    if (name != null) {
      query.where(Alias.$(itinerary.getName()).eq(name));
    }
    String desc = criteria.getDesc();
    if (desc != null) {
      query.where(Alias.$(itinerary.getDesc()).eq(desc));
    }
    Timestamp dateAndTime = criteria.getDateAndTime();
    if (dateAndTime != null) {
      query.where(Alias.$(itinerary.getCreationDate()).eq(dateAndTime));
    }
    addOrderBy(query, alias, itinerary, criteria.getSort());

    return findPaginated(criteria, query, alias);
  }

  @Override
  public List<ItineraryEntity> findAll() {

    return super.findAll();
  }

  private void addOrderBy(JPAQuery query, EntityPathBase<ItineraryEntity> alias, ItineraryEntity itinerary,
      List<OrderByTo> sort) {

    if (sort != null && !sort.isEmpty()) {
      for (OrderByTo orderEntry : sort) {
        switch (orderEntry.getName()) {
          case "name":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(itinerary.getName()).asc());
            } else {
              query.orderBy(Alias.$(itinerary.getName()).desc());
            }
            break;
          case "desc":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(itinerary.getDesc()).asc());
            } else {
              query.orderBy(Alias.$(itinerary.getDesc()).desc());
            }
            break;
          case "dateAndTime":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(itinerary.getCreationDate()).asc());
            } else {
              query.orderBy(Alias.$(itinerary.getCreationDate()).desc());
            }
            break;
        }
      }
    }
  }

}
