package com.cap.maas.itinerarymanagement.logic.api;

import java.util.List;

import com.cap.maas.itinerarymanagement.logic.api.to.ActivityCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivitySearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityStatusCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityStatusEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityStatusSearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeSearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.CustomerCto;
import com.cap.maas.itinerarymanagement.logic.api.to.CustomerEto;
import com.cap.maas.itinerarymanagement.logic.api.to.CustomerSearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItinerarySearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryStatusCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryStatusEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryStatusSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Interface for Itinerarymanagement component.
 */
public interface Itinerarymanagement {

  /**
   * Returns a Itinerary by its id 'id'.
   *
   * @param id The id 'id' of the Itinerary.
   * @return The {@link ItineraryEto} with id 'id'
   */
  ItineraryEto findItinerary(Long id);

  /**
   * Returns a paginated list of Itinerarys matching the search criteria.
   *
   * @param criteria the {@link ItinerarySearchCriteriaTo}.
   * @return the {@link List} of matching {@link ItineraryEto}s.
   */
  PaginatedListTo<ItineraryEto> findItineraryEtos(ItinerarySearchCriteriaTo criteria);

  /**
   * Deletes a itinerary from the database by its id 'itineraryId'.
   *
   * @param itineraryId Id of the itinerary to delete
   * @return boolean <code>true</code> if the itinerary can be deleted, <code>false</code> otherwise
   */
  boolean deleteItinerary(Long itineraryId);

  /**
   * Saves a itinerary and store it in the database.
   *
   * @param itinerary the {@link ItineraryEto} to create.
   * @return the new {@link ItineraryEto} that has been saved with ID and version.
   */
  ItineraryEto saveItinerary(ItineraryEto itinerary);

  /**
   * Saves a itinerary and store it in the database.
   *
   * @param itinerary the {@link ItineraryEto} to create.
   * @return the new {@link ItineraryEto} that has been saved with ID and version.
   */
  ItineraryEto saveItinerary(ItineraryCto itinerary);

  /**
   * Returns a composite Itinerary by its id 'id'.
   *
   * @param id The id 'id' of the Itinerary.
   * @return The {@link ItineraryCto} with id 'id'
   */
  ItineraryCto findItineraryCto(Long id);

  /**
   * Returns a paginated list of composite Itinerarys matching the search criteria.
   *
   * @param criteria the {@link ItinerarySearchCriteriaTo}.
   * @return the {@link List} of matching {@link ItineraryCto}s.
   */
  PaginatedListTo<ItineraryCto> findItineraryCtos(ItinerarySearchCriteriaTo criteria);

  /**
   * Returns a Activity by its id 'id'.
   *
   * @param id The id 'id' of the Activity.
   * @return The {@link ActivityEto} with id 'id'
   */
  ActivityEto findActivity(Long id);

  /**
   * Returns a paginated list of Activitys matching the search criteria.
   *
   * @param criteria the {@link ActivitySearchCriteriaTo}.
   * @return the {@link List} of matching {@link ActivityEto}s.
   */
  PaginatedListTo<ActivityEto> findActivityEtos(ActivitySearchCriteriaTo criteria);

  /**
   * Deletes a activity from the database by its id 'activityId'.
   *
   * @param activityId Id of the activity to delete
   * @return boolean <code>true</code> if the activity can be deleted, <code>false</code> otherwise
   */
  boolean deleteActivity(Long activityId);

  /**
   * Saves a activity and store it in the database.
   *
   * @param activity the {@link ActivityEto} to create.
   * @return the new {@link ActivityEto} that has been saved with ID and version.
   */
  ActivityEto saveActivity(ActivityEto activity);

  ActivityEto updateActivity(Long id, Long statusId);

  /**
   * Returns a composite Activity by its id 'id'.
   *
   * @param id The id 'id' of the Activity.
   * @return The {@link ActivityCto} with id 'id'
   */
  ActivityCto findActivityCto(Long id);

  /**
   * Returns a paginated list of composite Activitys matching the search criteria.
   *
   * @param criteria the {@link ActivitySearchCriteriaTo}.
   * @return the {@link List} of matching {@link ActivityCto}s.
   */
  PaginatedListTo<ActivityCto> findActivityCtos(ActivitySearchCriteriaTo criteria);

  /**
   * Returns a ActivityType by its id 'id'.
   *
   * @param id The id 'id' of the ActivityType.
   * @return The {@link ActivityTypeEto} with id 'id'
   */
  ActivityTypeEto findActivityType(Long id);

  /**
   * Returns a paginated list of ActivityTypes matching the search criteria.
   *
   * @param criteria the {@link ActivityTypeSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ActivityTypeEto}s.
   */
  PaginatedListTo<ActivityTypeEto> findActivityTypeEtos(ActivityTypeSearchCriteriaTo criteria);

  /**
   * Deletes a activityType from the database by its id 'activityTypeId'.
   *
   * @param activityTypeId Id of the activityType to delete
   * @return boolean <code>true</code> if the activityType can be deleted, <code>false</code> otherwise
   */
  boolean deleteActivityType(Long activityTypeId);

  /**
   * Saves a activityType and store it in the database.
   *
   * @param activityType the {@link ActivityTypeEto} to create.
   * @return the new {@link ActivityTypeEto} that has been saved with ID and version.
   */
  ActivityTypeEto saveActivityType(ActivityTypeEto activityType);

  /**
   * Returns a composite ActivityType by its id 'id'.
   *
   * @param id The id 'id' of the ActivityType.
   * @return The {@link ActivityTypeCto} with id 'id'
   */
  ActivityTypeCto findActivityTypeCto(Long id);

  /**
   * Returns a paginated list of composite ActivityTypes matching the search criteria.
   *
   * @param criteria the {@link ActivityTypeSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ActivityTypeCto}s.
   */
  PaginatedListTo<ActivityTypeCto> findActivityTypeCtos(ActivityTypeSearchCriteriaTo criteria);

  /**
   * Returns a ActivityStatus by its id 'id'.
   *
   * @param id The id 'id' of the ActivityStatus.
   * @return The {@link ActivityStatusEto} with id 'id'
   */
  ActivityStatusEto findActivityStatus(Long id);

  /**
   * Returns a paginated list of ActivityStatuss matching the search criteria.
   *
   * @param criteria the {@link ActivityStatusSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ActivityStatusEto}s.
   */
  PaginatedListTo<ActivityStatusEto> findActivityStatusEtos(ActivityStatusSearchCriteriaTo criteria);

  /**
   * Deletes a activityStatus from the database by its id 'activityStatusId'.
   *
   * @param activityStatusId Id of the activityStatus to delete
   * @return boolean <code>true</code> if the activityStatus can be deleted, <code>false</code> otherwise
   */
  boolean deleteActivityStatus(Long activityStatusId);

  /**
   * Saves a activityStatus and store it in the database.
   *
   * @param activityStatus the {@link ActivityStatusEto} to create.
   * @return the new {@link ActivityStatusEto} that has been saved with ID and version.
   */
  ActivityStatusEto saveActivityStatus(ActivityStatusEto activityStatus);

  /**
   * Returns a composite ActivityStatus by its id 'id'.
   *
   * @param id The id 'id' of the ActivityStatus.
   * @return The {@link ActivityStatusCto} with id 'id'
   */
  ActivityStatusCto findActivityStatusCto(Long id);

  /**
   * Returns a paginated list of composite ActivityStatuss matching the search criteria.
   *
   * @param criteria the {@link ActivityStatusSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ActivityStatusCto}s.
   */
  PaginatedListTo<ActivityStatusCto> findActivityStatusCtos(ActivityStatusSearchCriteriaTo criteria);

  void reset(Long id);

  ActivityEto book(Long id, Long servId);

  /**
   * Returns a Customer by its id 'id'.
   *
   * @param id The id 'id' of the Customer.
   * @return The {@link CustomerEto} with id 'id'
   */
  CustomerEto findCustomer(Long id);

  /**
   * Returns a paginated list of Customers matching the search criteria.
   *
   * @param criteria the {@link CustomerSearchCriteriaTo}.
   * @return the {@link List} of matching {@link CustomerEto}s.
   */
  PaginatedListTo<CustomerEto> findCustomerEtos(CustomerSearchCriteriaTo criteria);

  /**
   * Deletes a customer from the database by its id 'customerId'.
   *
   * @param customerId Id of the customer to delete
   * @return boolean <code>true</code> if the customer can be deleted, <code>false</code> otherwise
   */
  boolean deleteCustomer(Long customerId);

  /**
   * Saves a customer and store it in the database.
   *
   * @param customer the {@link CustomerEto} to create.
   * @return the new {@link CustomerEto} that has been saved with ID and version.
   */
  CustomerEto saveCustomer(CustomerEto customer);

  /**
   * Returns a composite Customer by its id 'id'.
   *
   * @param id The id 'id' of the Customer.
   * @return The {@link CustomerCto} with id 'id'
   */
  CustomerCto findCustomerCto(Long id);

  /**
   * Returns a paginated list of composite Customers matching the search criteria.
   *
   * @param criteria the {@link CustomerSearchCriteriaTo}.
   * @return the {@link List} of matching {@link CustomerCto}s.
   */
  PaginatedListTo<CustomerCto> findCustomerCtos(CustomerSearchCriteriaTo criteria);

  /**
   * Returns a ItineraryStatus by its id 'id'.
   *
   * @param id The id 'id' of the ItineraryStatus.
   * @return The {@link ItineraryStatusEto} with id 'id'
   */
  ItineraryStatusEto findItineraryStatus(Long id);

  /**
   * Returns a paginated list of ItineraryStatuss matching the search criteria.
   *
   * @param criteria the {@link ItineraryStatusSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ItineraryStatusEto}s.
   */
  PaginatedListTo<ItineraryStatusEto> findItineraryStatusEtos(ItineraryStatusSearchCriteriaTo criteria);

  /**
   * Deletes a itineraryStatus from the database by its id 'itineraryStatusId'.
   *
   * @param itineraryStatusId Id of the itineraryStatus to delete
   * @return boolean <code>true</code> if the itineraryStatus can be deleted, <code>false</code> otherwise
   */
  boolean deleteItineraryStatus(Long itineraryStatusId);

  /**
   * Saves a itineraryStatus and store it in the database.
   *
   * @param itineraryStatus the {@link ItineraryStatusEto} to create.
   * @return the new {@link ItineraryStatusEto} that has been saved with ID and version.
   */
  ItineraryStatusEto saveItineraryStatus(ItineraryStatusEto itineraryStatus);

  /**
   * Returns a composite ItineraryStatus by its id 'id'.
   *
   * @param id The id 'id' of the ItineraryStatus.
   * @return The {@link ItineraryStatusCto} with id 'id'
   */
  ItineraryStatusCto findItineraryStatusCto(Long id);

  /**
   * Returns a paginated list of composite ItineraryStatuss matching the search criteria.
   *
   * @param criteria the {@link ItineraryStatusSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ItineraryStatusCto}s.
   */
  PaginatedListTo<ItineraryStatusCto> findItineraryStatusCtos(ItineraryStatusSearchCriteriaTo criteria);

}
