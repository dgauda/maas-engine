package com.cap.maas.itinerarymanagement.dataaccess.api.dao;

import java.util.List;

import com.cap.maas.general.dataaccess.api.dao.ApplicationDao;
import com.cap.maas.itinerarymanagement.dataaccess.api.ItineraryEntity;
import com.cap.maas.itinerarymanagement.logic.api.to.ItinerarySearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for Itinerary entities
 */
public interface ItineraryDao extends ApplicationDao<ItineraryEntity> {

  /**
   * Finds the {@link ItineraryEntity itinerarys} matching the given {@link ItinerarySearchCriteriaTo}.
   *
   * @param criteria is the {@link ItinerarySearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link ItineraryEntity} objects.
   */
  PaginatedListTo<ItineraryEntity> findItinerarys(ItinerarySearchCriteriaTo criteria);

  public List<ItineraryEntity> findAll();

}
