package com.cap.maas.itinerarymanagement.logic.impl;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.cap.maas.general.common.api.constants.ApplicationConstants;
import com.cap.maas.general.common.util.MaasEngineUtil;
import com.cap.maas.general.logic.base.AbstractComponentFacade;
import com.cap.maas.itinerarymanagement.dataaccess.api.ActivityEntity;
import com.cap.maas.itinerarymanagement.dataaccess.api.ActivityStatusEntity;
import com.cap.maas.itinerarymanagement.dataaccess.api.ActivityTypeEntity;
import com.cap.maas.itinerarymanagement.dataaccess.api.CustomerEntity;
import com.cap.maas.itinerarymanagement.dataaccess.api.ItineraryEntity;
import com.cap.maas.itinerarymanagement.dataaccess.api.ItineraryStatusEntity;
import com.cap.maas.itinerarymanagement.dataaccess.api.dao.ActivityDao;
import com.cap.maas.itinerarymanagement.dataaccess.api.dao.ActivityStatusDao;
import com.cap.maas.itinerarymanagement.dataaccess.api.dao.ActivityTypeDao;
import com.cap.maas.itinerarymanagement.dataaccess.api.dao.CustomerDao;
import com.cap.maas.itinerarymanagement.dataaccess.api.dao.ItineraryDao;
import com.cap.maas.itinerarymanagement.dataaccess.api.dao.ItineraryStatusDao;
import com.cap.maas.itinerarymanagement.logic.api.Itinerarymanagement;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivitySearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityStatusCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityStatusEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityStatusSearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeSearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.CustomerCto;
import com.cap.maas.itinerarymanagement.logic.api.to.CustomerEto;
import com.cap.maas.itinerarymanagement.logic.api.to.CustomerSearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItinerarySearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryStatusCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryStatusEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryStatusSearchCriteriaTo;
import com.cap.maas.registrationmanagement.dataaccess.api.RegistrationEntity;
import com.cap.maas.registrationmanagement.dataaccess.api.dao.RegistrationDao;
import com.cap.maas.registrationmanagement.logic.api.Registrationmanagement;
import com.cap.maas.registrationmanagement.logic.api.to.RegistrationEto;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;
import io.oasp.module.jpa.common.api.to.PaginationResultTo;

/**
 * Implementation of component interface of itinerarymanagement
 */
@Named
@Transactional
public class ItinerarymanagementImpl extends AbstractComponentFacade implements Itinerarymanagement {

  /**
   * Logger instance.
   */
  private static final Logger LOG = LoggerFactory.getLogger(ItinerarymanagementImpl.class);

  /**
   * @see #getItineraryDao()
   */
  @Inject
  private ItineraryDao itineraryDao;

  /**
   * @see #getActivityDao()
   */
  @Inject
  private ActivityDao activityDao;

  @Inject
  private ActivityTypeDao activityTypeDao;

  /**
   * @see #getRegistrationDao()
   */
  @Inject
  private RegistrationDao registrationDao;

  /**
   * @see #getActivityStatusDao()
   */
  @Inject
  private ActivityStatusDao activityStatusDao;

  @Inject
  private Registrationmanagement registrationManagement;

  /**
   * @see #getCustomerDao()
   */
  @Inject
  private CustomerDao customerDao;

  /**
   * @see #getItineraryStatusDao()
   */
  @Inject
  private ItineraryStatusDao itineraryStatusDao;

  /**
   * The constructor.
   */
  public ItinerarymanagementImpl() {

    super();
  }

  @Override
  public ItineraryEto findItinerary(Long id) {

    LOG.debug("Get Itinerary with id {} from database.", id);
    return getBeanMapper().map(getItineraryDao().findOne(id), ItineraryEto.class);
  }

  @Override
  public PaginatedListTo<ItineraryEto> findItineraryEtos(ItinerarySearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ItineraryEntity> itinerarys = getItineraryDao().findItinerarys(criteria);
    return mapPaginatedEntityList(itinerarys, ItineraryEto.class);
  }

  @Override
  public boolean deleteItinerary(Long itineraryId) {

    ItineraryEntity itinerary = getItineraryDao().find(itineraryId);
    getItineraryDao().delete(itinerary);
    LOG.debug("The itinerary with id '{}' has been deleted.", itineraryId);
    return true;
  }

  @Override
  public ItineraryEto saveItinerary(ItineraryEto itinerary) {

    Objects.requireNonNull(itinerary, "itinerary");
    ItineraryEntity itineraryEntity = getBeanMapper().map(itinerary, ItineraryEntity.class);

    // initialize, validate itineraryEntity here if necessary
    ItineraryEntity resultEntity = getItineraryDao().save(itineraryEntity);
    LOG.debug("Itinerary with id '{}' has been created.", resultEntity.getId());

    return getBeanMapper().map(resultEntity, ItineraryEto.class);
  }

  @Override
  public ItineraryEto saveItinerary(ItineraryCto itinerary) {

    Objects.requireNonNull(itinerary, "itinerary");
    ItineraryEntity itineraryEntity = getBeanMapper().map(itinerary.getItinerary(), ItineraryEntity.class);
    LOG.debug("itineraryEntity" + itineraryEntity);

    List<ActivityEntity> activities = getBeanMapper().mapList(itinerary.getActivities(), ActivityEntity.class);
    LOG.debug("activities" + activities);
    itineraryEntity.setActivities(activities);

    ItineraryStatusEntity itineraryStatusEntity = getItineraryStatusDao().findOne(itineraryEntity.getStatus().getId());
    itineraryEntity.setStatus(itineraryStatusEntity);

    itineraryEntity.setCreationDate(Timestamp.from(Instant.now()));

    // initialize, validate itineraryEntity here if necessary
    ItineraryEntity resultEntity = getItineraryDao().save(itineraryEntity);
    LOG.debug("Itinerary with id '{}' has been created.", resultEntity.getId());

    for (ActivityEntity activity : resultEntity.getActivities()) {
      activity.setItineraryId(resultEntity.getId());

      RegistrationEntity registration = getRegistrationDao().findOne(activity.getRegistrationId());

      activity.setRegistrationEntity(registration);

      ActivityStatusEntity activityStatus = getActivityStatusDao().findOne(activity.getStatusId());

      activity.setStatus(activityStatus);

      ActivityEntity resultActivity = getActivityDao().save(activity);

      LOG.info("Activity with id '{}' has been created.", resultActivity.getId());
    }

    return getBeanMapper().map(resultEntity, ItineraryEto.class);
  }

  /**
   * Returns the field 'itineraryDao'.
   *
   * @return the {@link ItineraryDao} instance.
   */
  public ItineraryDao getItineraryDao() {

    return this.itineraryDao;
  }

  public RegistrationDao getRegistrationDao() {

    return this.registrationDao;
  }

  @Override
  public ItineraryCto findItineraryCto(Long id) {

    LOG.debug("Get ItineraryCto with id {} from database.", id);
    ItineraryEntity entity = getItineraryDao().findOne(id);
    
    if( entity==null )  //TODO throw new RuntimeException("NotFound");
    	return null;
    	
    ItineraryCto cto = new ItineraryCto();
    cto.setItinerary(getBeanMapper().map(entity, ItineraryEto.class));
    cto.setActivities(getBeanMapper().mapList(entity.getActivities(), ActivityEto.class));
    cto.setCustomer(getBeanMapper().map(entity.getCustomer(), CustomerEto.class));
    cto.setStatus(getBeanMapper().map(entity.getStatus(), ItineraryStatusEto.class));

    return cto;
  }

  @Override
  public PaginatedListTo<ItineraryCto> findItineraryCtos(ItinerarySearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ItineraryEntity> itinerarys = getItineraryDao().findItinerarys(criteria);
    List<ItineraryCto> ctos = new ArrayList<>();
    for (ItineraryEntity entity : itinerarys.getResult()) {
      ItineraryCto cto = new ItineraryCto();
      cto.setItinerary(getBeanMapper().map(entity, ItineraryEto.class));
      cto.setActivities(getBeanMapper().mapList(entity.getActivities(), ActivityEto.class));
      ctos.add(cto);

    }
    PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
    PaginatedListTo<ItineraryCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
    return pagListTo;
  }

  @Override
  public ActivityEto findActivity(Long id) {

    LOG.debug("Get Activity with id {} from database.", id);
    return getBeanMapper().map(getActivityDao().findOne(id), ActivityEto.class);
  }

  @Override
  public PaginatedListTo<ActivityEto> findActivityEtos(ActivitySearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ActivityEntity> activitys = getActivityDao().findActivitys(criteria);
    return mapPaginatedEntityList(activitys, ActivityEto.class);
  }

  @Override
  public boolean deleteActivity(Long activityId) {

    ActivityEntity activity = getActivityDao().find(activityId);
    getActivityDao().delete(activity);
    LOG.debug("The activity with id '{}' has been deleted.", activityId);
    return true;
  }

  @Override
  public ActivityEto saveActivity(ActivityEto activity) {

    Objects.requireNonNull(activity, "activity");
    ActivityEntity activityEntity = getBeanMapper().map(activity, ActivityEntity.class);

    // initialize, validate activityEntity here if necessary
    ActivityEntity resultEntity = getActivityDao().save(activityEntity);
    LOG.debug("Activity with id '{}' has been created.", resultEntity.getId());

    return getBeanMapper().map(resultEntity, ActivityEto.class);
  }

  @Override
  public ActivityEto updateActivity(Long id, Long statusId) {

    ActivityEntity entity = getActivityDao().findOne(id);

    entity.setStatus(getActivityStatusDao().find(statusId));

    // initialize, validate activityEntity here if necessary
    ActivityEntity resultEntity = getActivityDao().save(entity);
    LOG.debug("Activity with id '{}' has been created.", resultEntity.getId());

    return getBeanMapper().map(resultEntity, ActivityEto.class);
  }

  /**
   * Returns the field 'activityDao'.
   *
   * @return the {@link ActivityDao} instance.
   */
  public ActivityDao getActivityDao() {

    return this.activityDao;
  }

  @Override
  public ActivityCto findActivityCto(Long id) {

    LOG.debug("Get ActivityCto with id {} from database.", id);
    ActivityEntity entity = getActivityDao().findOne(id);
    ActivityCto cto = new ActivityCto();
    cto.setActivity(getBeanMapper().map(entity, ActivityEto.class));
    cto.setRegistrationEto(getBeanMapper().map(entity.getRegistrationEntity(), RegistrationEto.class));
    cto.setStatus(getBeanMapper().map(entity.getStatus(), ActivityStatusEto.class));
    cto.setItinerary(getBeanMapper().map(entity.getItinerary(), ItineraryEto.class));

    return cto;
  }

  @Override
  public PaginatedListTo<ActivityCto> findActivityCtos(ActivitySearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ActivityEntity> activitys = getActivityDao().findActivitys(criteria);
    List<ActivityCto> ctos = new ArrayList<>();
    for (ActivityEntity entity : activitys.getResult()) {
      ActivityCto cto = new ActivityCto();
      cto.setActivity(getBeanMapper().map(entity, ActivityEto.class));
      cto.setRegistrationEto(getBeanMapper().map(entity.getRegistrationEntity(), RegistrationEto.class));
      cto.setItinerary(getBeanMapper().map(entity.getItinerary(), ItineraryEto.class));
      ctos.add(cto);

    }
    PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
    PaginatedListTo<ActivityCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
    return pagListTo;
  }

  @Override
  public ActivityTypeEto findActivityType(Long id) {

    LOG.debug("Get ActivityType with id {} from database.", id);
    return getBeanMapper().map(getActivityTypeDao().findOne(id), ActivityTypeEto.class);
  }

  @Override
  public PaginatedListTo<ActivityTypeEto> findActivityTypeEtos(ActivityTypeSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ActivityTypeEntity> activitytypes = getActivityTypeDao().findActivityTypes(criteria);
    return mapPaginatedEntityList(activitytypes, ActivityTypeEto.class);
  }

  @Override
  public boolean deleteActivityType(Long activityTypeId) {

    ActivityTypeEntity activityType = getActivityTypeDao().find(activityTypeId);
    getActivityTypeDao().delete(activityType);
    LOG.debug("The activityType with id '{}' has been deleted.", activityTypeId);
    return true;
  }

  @Override
  public ActivityTypeEto saveActivityType(ActivityTypeEto activityType) {

    Objects.requireNonNull(activityType, "activityType");
    ActivityTypeEntity activityTypeEntity = getBeanMapper().map(activityType, ActivityTypeEntity.class);

    // initialize, validate activityTypeEntity here if necessary
    ActivityTypeEntity resultEntity = getActivityTypeDao().save(activityTypeEntity);
    LOG.debug("ActivityType with id '{}' has been created.", resultEntity.getId());

    return getBeanMapper().map(resultEntity, ActivityTypeEto.class);
  }

  /**
   * Returns the field 'activityTypeDao'.
   *
   * @return the {@link ActivityTypeDao} instance.
   */
  public ActivityTypeDao getActivityTypeDao() {

    return this.activityTypeDao;
  }

  @Override
  public ActivityTypeCto findActivityTypeCto(Long id) {

    LOG.debug("Get ActivityTypeCto with id {} from database.", id);
    ActivityTypeEntity entity = getActivityTypeDao().findOne(id);
    ActivityTypeCto cto = new ActivityTypeCto();
    cto.setActivityType(getBeanMapper().map(entity, ActivityTypeEto.class));

    return cto;
  }

  @Override
  public PaginatedListTo<ActivityTypeCto> findActivityTypeCtos(ActivityTypeSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ActivityTypeEntity> activitytypes = getActivityTypeDao().findActivityTypes(criteria);
    List<ActivityTypeCto> ctos = new ArrayList<>();
    for (ActivityTypeEntity entity : activitytypes.getResult()) {
      ActivityTypeCto cto = new ActivityTypeCto();
      cto.setActivityType(getBeanMapper().map(entity, ActivityTypeEto.class));
      ctos.add(cto);

    }
    PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
    PaginatedListTo<ActivityTypeCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
    return pagListTo;
  }

  @Override
  public ActivityStatusEto findActivityStatus(Long id) {

    LOG.debug("Get ActivityStatus with id {} from database.", id);
    return getBeanMapper().map(getActivityStatusDao().findOne(id), ActivityStatusEto.class);
  }

  @Override
  public PaginatedListTo<ActivityStatusEto> findActivityStatusEtos(ActivityStatusSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ActivityStatusEntity> activitystatuss = getActivityStatusDao().findActivityStatuss(criteria);
    return mapPaginatedEntityList(activitystatuss, ActivityStatusEto.class);
  }

  @Override
  public boolean deleteActivityStatus(Long activityStatusId) {

    ActivityStatusEntity activityStatus = getActivityStatusDao().find(activityStatusId);
    getActivityStatusDao().delete(activityStatus);
    LOG.debug("The activityStatus with id '{}' has been deleted.", activityStatusId);
    return true;
  }

  @Override
  public ActivityStatusEto saveActivityStatus(ActivityStatusEto activityStatus) {

    Objects.requireNonNull(activityStatus, "activityStatus");
    ActivityStatusEntity activityStatusEntity = getBeanMapper().map(activityStatus, ActivityStatusEntity.class);

    // initialize, validate activityStatusEntity here if necessary
    ActivityStatusEntity resultEntity = getActivityStatusDao().save(activityStatusEntity);
    LOG.debug("ActivityStatus with id '{}' has been created.", resultEntity.getId());

    return getBeanMapper().map(resultEntity, ActivityStatusEto.class);
  }

  /**
   * Returns the field 'activityStatusDao'.
   *
   * @return the {@link ActivityStatusDao} instance.
   */
  public ActivityStatusDao getActivityStatusDao() {

    return this.activityStatusDao;
  }

  @Override
  public ActivityStatusCto findActivityStatusCto(Long id) {

    LOG.debug("Get ActivityStatusCto with id {} from database.", id);
    ActivityStatusEntity entity = getActivityStatusDao().findOne(id);
    ActivityStatusCto cto = new ActivityStatusCto();
    cto.setActivityStatus(getBeanMapper().map(entity, ActivityStatusEto.class));

    return cto;
  }

  @Override
  public PaginatedListTo<ActivityStatusCto> findActivityStatusCtos(ActivityStatusSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ActivityStatusEntity> activitystatuss = getActivityStatusDao().findActivityStatuss(criteria);
    List<ActivityStatusCto> ctos = new ArrayList<>();
    for (ActivityStatusEntity entity : activitystatuss.getResult()) {
      ActivityStatusCto cto = new ActivityStatusCto();
      cto.setActivityStatus(getBeanMapper().map(entity, ActivityStatusEto.class));
      ctos.add(cto);

    }
    PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
    PaginatedListTo<ActivityStatusCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
    return pagListTo;
  }

  public void reset(Long id) {

    LOG.info(" Inside reset '{}' ", id);

    ActivitySearchCriteriaTo criteria = new ActivitySearchCriteriaTo();
    criteria.setItineraryId(id);

    List<ActivityEto> list = findActivityEtos(criteria).getResult();

    for (ActivityEto eto : list) {
      updateActivity(eto.getId(), Long.valueOf(1));
    }
  }

  public ActivityEto book(Long id, Long regServId) {

    RegistrationEto regEto = this.registrationManagement.findRegistration(regServId);
    LOG.info("regEto '{}'", regEto);

    ActivityEto resultEto = null;

    ActivitySearchCriteriaTo criteria = new ActivitySearchCriteriaTo();
    criteria.setItineraryId(id);
    criteria.setRegistrationId(regServId);
    criteria.setStatusId(ApplicationConstants.ACTIVITYSTATUS_PENDING);

    List<OrderByTo> orderByList = new ArrayList<OrderByTo>();
    OrderByTo orderBy = new OrderByTo();
    orderBy.setName("priority");
    orderBy.setDirection(OrderDirection.ASC);
    orderByList.add(orderBy);
    criteria.setSort(orderByList);

    List<ActivityEto> list = findActivityEtos(criteria).getResult();

    if (!list.isEmpty()) {
      ActivityEto actEto = list.get(0);

      LOG.info("list '{}'", list);

      if (actEto.getRegistrationId().equals(regEto.getId())) {

        String srvUrl = regEto.getServiceUrl();
        LOG.info("srvUrl '{}' ", srvUrl);

        Long IdCustomer = findItinerary(actEto.getItineraryId()).getCustomerId();
        LOG.info("IdCustomer '{}' ", IdCustomer);

        try {
          JSONObject json = new JSONObject(); /* To be changed later */
          json.put("noOfTickets", "7"); // ?, use payload logic
          json.put("showidId", "1"); // ?, use payload logic
          json.put("customeridId", IdCustomer);

          String response = MaasEngineUtil.postCall(srvUrl, json);

        } catch (JSONException e) {
          LOG.error(e.getMessage());
          e.printStackTrace();
        }

        resultEto = updateActivity(actEto.getId(), ApplicationConstants.ACTIVITYSTATUS_COMPLETE);
      }
    }

    return resultEto;
  }

  @Override
  public CustomerEto findCustomer(Long id) {

    LOG.debug("Get Customer with id {} from database.", id);
    return getBeanMapper().map(getCustomerDao().findOne(id), CustomerEto.class);
  }

  @Override
  public PaginatedListTo<CustomerEto> findCustomerEtos(CustomerSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<CustomerEntity> customers = getCustomerDao().findCustomers(criteria);
    return mapPaginatedEntityList(customers, CustomerEto.class);
  }

  @Override
  public boolean deleteCustomer(Long customerId) {

    CustomerEntity customer = getCustomerDao().find(customerId);
    getCustomerDao().delete(customer);
    LOG.debug("The customer with id '{}' has been deleted.", customerId);
    return true;
  }

  @Override
  public CustomerEto saveCustomer(CustomerEto customer) {

    Objects.requireNonNull(customer, "customer");
    CustomerEntity customerEntity = getBeanMapper().map(customer, CustomerEntity.class);

    // initialize, validate customerEntity here if necessary
    CustomerEntity resultEntity = getCustomerDao().save(customerEntity);
    LOG.debug("Customer with id '{}' has been created.", resultEntity.getId());

    return getBeanMapper().map(resultEntity, CustomerEto.class);
  }

  /**
   * Returns the field 'customerDao'.
   *
   * @return the {@link CustomerDao} instance.
   */
  public CustomerDao getCustomerDao() {

    return this.customerDao;
  }

  @Override
  public CustomerCto findCustomerCto(Long id) {

    LOG.debug("Get CustomerCto with id {} from database.", id);
    CustomerEntity entity = getCustomerDao().findOne(id);
    CustomerCto cto = new CustomerCto();
    cto.setCustomer(getBeanMapper().map(entity, CustomerEto.class));
    cto.setItineraries(getBeanMapper().mapList(entity.getItineraries(), ItineraryEto.class));

    return cto;
  }

  @Override
  public PaginatedListTo<CustomerCto> findCustomerCtos(CustomerSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<CustomerEntity> customers = getCustomerDao().findCustomers(criteria);
    List<CustomerCto> ctos = new ArrayList<>();
    for (CustomerEntity entity : customers.getResult()) {
      CustomerCto cto = new CustomerCto();
      cto.setCustomer(getBeanMapper().map(entity, CustomerEto.class));
      cto.setItineraries(getBeanMapper().mapList(entity.getItineraries(), ItineraryEto.class));
      ctos.add(cto);

    }
    PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
    PaginatedListTo<CustomerCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
    return pagListTo;
  }

  @Override
  public ItineraryStatusEto findItineraryStatus(Long id) {

    LOG.debug("Get ItineraryStatus with id {} from database.", id);
    return getBeanMapper().map(getItineraryStatusDao().findOne(id), ItineraryStatusEto.class);
  }

  @Override
  public PaginatedListTo<ItineraryStatusEto> findItineraryStatusEtos(ItineraryStatusSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ItineraryStatusEntity> itinerarystatuss = getItineraryStatusDao().findItineraryStatuss(criteria);
    return mapPaginatedEntityList(itinerarystatuss, ItineraryStatusEto.class);
  }

  @Override
  public boolean deleteItineraryStatus(Long itineraryStatusId) {

    ItineraryStatusEntity itineraryStatus = getItineraryStatusDao().find(itineraryStatusId);
    getItineraryStatusDao().delete(itineraryStatus);
    LOG.debug("The itineraryStatus with id '{}' has been deleted.", itineraryStatusId);
    return true;
  }

  @Override
  public ItineraryStatusEto saveItineraryStatus(ItineraryStatusEto itineraryStatus) {

    Objects.requireNonNull(itineraryStatus, "itineraryStatus");
    ItineraryStatusEntity itineraryStatusEntity = getBeanMapper().map(itineraryStatus, ItineraryStatusEntity.class);

    // initialize, validate itineraryStatusEntity here if necessary
    ItineraryStatusEntity resultEntity = getItineraryStatusDao().save(itineraryStatusEntity);
    LOG.debug("ItineraryStatus with id '{}' has been created.", resultEntity.getId());

    return getBeanMapper().map(resultEntity, ItineraryStatusEto.class);
  }

  /**
   * Returns the field 'itineraryStatusDao'.
   *
   * @return the {@link ItineraryStatusDao} instance.
   */
  public ItineraryStatusDao getItineraryStatusDao() {

    return this.itineraryStatusDao;
  }

  @Override
  public ItineraryStatusCto findItineraryStatusCto(Long id) {

    LOG.debug("Get ItineraryStatusCto with id {} from database.", id);
    ItineraryStatusEntity entity = getItineraryStatusDao().findOne(id);
    ItineraryStatusCto cto = new ItineraryStatusCto();
    cto.setItineraryStatus(getBeanMapper().map(entity, ItineraryStatusEto.class));

    return cto;
  }

  @Override
  public PaginatedListTo<ItineraryStatusCto> findItineraryStatusCtos(ItineraryStatusSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ItineraryStatusEntity> itinerarystatuss = getItineraryStatusDao().findItineraryStatuss(criteria);
    List<ItineraryStatusCto> ctos = new ArrayList<>();
    for (ItineraryStatusEntity entity : itinerarystatuss.getResult()) {
      ItineraryStatusCto cto = new ItineraryStatusCto();
      cto.setItineraryStatus(getBeanMapper().map(entity, ItineraryStatusEto.class));
      ctos.add(cto);

    }
    PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
    PaginatedListTo<ItineraryStatusCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
    return pagListTo;
  }

}
