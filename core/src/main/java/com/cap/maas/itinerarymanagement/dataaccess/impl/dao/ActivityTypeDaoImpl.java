package com.cap.maas.itinerarymanagement.dataaccess.impl.dao;

import java.util.List;

import javax.inject.Named;

import com.cap.maas.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.cap.maas.itinerarymanagement.dataaccess.api.ActivityTypeEntity;
import com.cap.maas.itinerarymanagement.dataaccess.api.dao.ActivityTypeDao;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeSearchCriteriaTo;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link ActivityTypeDao}.
 */
@Named
public class ActivityTypeDaoImpl extends ApplicationDaoImpl<ActivityTypeEntity> implements ActivityTypeDao {

  /**
   * The constructor.
   */
  public ActivityTypeDaoImpl() {

    super();
  }

  @Override
  public Class<ActivityTypeEntity> getEntityClass() {

    return ActivityTypeEntity.class;
  }

  @Override
  public PaginatedListTo<ActivityTypeEntity> findActivityTypes(ActivityTypeSearchCriteriaTo criteria) {

    ActivityTypeEntity activitytype = Alias.alias(ActivityTypeEntity.class);
    EntityPathBase<ActivityTypeEntity> alias = Alias.$(activitytype);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    String name = criteria.getName();
    if (name != null) {
      query.where(Alias.$(activitytype.getName()).eq(name));
    }
    Boolean active = criteria.getActive();
    if (active != null) {
      query.where(Alias.$(activitytype.getActive()).eq(active));
    }
    addOrderBy(query, alias, activitytype, criteria.getSort());

    return findPaginated(criteria, query, alias);
  }

  private void addOrderBy(JPAQuery query, EntityPathBase<ActivityTypeEntity> alias, ActivityTypeEntity activitytype,
      List<OrderByTo> sort) {

    if (sort != null && !sort.isEmpty()) {
      for (OrderByTo orderEntry : sort) {
        switch (orderEntry.getName()) {
          case "name":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(activitytype.getName()).asc());
            } else {
              query.orderBy(Alias.$(activitytype.getName()).desc());
            }
            break;
          case "active":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(activitytype.getActive()).asc());
            } else {
              query.orderBy(Alias.$(activitytype.getActive()).desc());
            }
            break;
        }
      }
    }
  }

}