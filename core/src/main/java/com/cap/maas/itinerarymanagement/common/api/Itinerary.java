package com.cap.maas.itinerarymanagement.common.api;

import java.sql.Timestamp;

import com.cap.maas.general.common.api.ApplicationEntity;

public interface Itinerary extends ApplicationEntity {

  public String getName();

  public void setName(String name);

  public String getDesc();

  public void setDesc(String desc);

  public Timestamp getCreationDate();

  public void setCreationDate(Timestamp creationDate);

  public Long getCustomerId();

  public void setCustomerId(Long customerId);

  public Long getStatusId();

  public void setStatusId(Long statusId);

}
