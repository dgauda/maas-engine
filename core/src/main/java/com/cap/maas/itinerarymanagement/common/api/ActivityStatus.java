package com.cap.maas.itinerarymanagement.common.api;

import com.cap.maas.general.common.api.ApplicationEntity;

public interface ActivityStatus extends ApplicationEntity {

  public String getName();

  public void setName(String name);

  public String getDesc();

  public void setDesc(String desc);

}
