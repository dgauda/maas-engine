package com.cap.maas.itinerarymanagement.common.api;

import java.sql.Timestamp;

import com.cap.maas.general.common.api.ApplicationEntity;

public interface Customer extends ApplicationEntity {

  public String getName();

  public void setName(String name);

  public int getContactnumber();

  public void setContactnumber(int contactnumber);

  public boolean isIsregularcustomer();

  public void setIsregularcustomer(boolean isregularcustomer);

  public Timestamp getRegistrationDate();

  public void setRegistrationDate(Timestamp registrationDate);

}
