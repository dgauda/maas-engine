package com.cap.maas.itinerarymanagement.dataaccess.api;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cap.maas.general.dataaccess.api.ApplicationPersistenceEntity;
import com.cap.maas.itinerarymanagement.common.api.Itinerary;

/**
 * @author dgauda
 */
@Entity
@Table(name = "Itinerary")
public class ItineraryEntity extends ApplicationPersistenceEntity implements Itinerary {

  private String name;

  private String desc;

  @Temporal(TemporalType.TIMESTAMP)
  private Timestamp creationDate;

  private List<ActivityEntity> activities;

  private CustomerEntity customer;

  private ItineraryStatusEntity status;

  private static final long serialVersionUID = 1L;

  /**
   * @return status
   */
  @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "idItinStatus")
  public ItineraryStatusEntity getStatus() {

    return this.status;
  }

  /**
   * @param status new value of {@link #getstatus}.
   */
  public void setStatus(ItineraryStatusEntity status) {

    this.status = status;
  }

  /**
   * @return customer
   */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "idCustomer")
  public CustomerEntity getCustomer() {

    return this.customer;
  }

  /**
   * @param customer new value of {@link #getcustomer}.
   */
  public void setCustomer(CustomerEntity customer) {

    this.customer = customer;
  }

  /**
   * @return name
   */
  public String getName() {

    return this.name;
  }

  /**
   * @param name new value of {@link #getname}.
   */
  public void setName(String name) {

    this.name = name;
  }

  /**
   * @return desc
   */
  public String getDesc() {

    return this.desc;
  }

  /**
   * @param desc new value of {@link #getdesc}.
   */
  public void setDesc(String desc) {

    this.desc = desc;
  }

  /**
   * @return dateAndTime
   */
  public Timestamp getCreationDate() {

    return this.creationDate;
  }

  /**
   * @param dateAndTime new value of {@link #getdateAndTime}.
   */
  public void setCreationDate(Timestamp creationDate) {

    this.creationDate = creationDate;
  }

  /**
   * @return actvities
   */
  @OneToMany(mappedBy = "itinerary", fetch = FetchType.LAZY)
  public List<ActivityEntity> getActivities() {

    return this.activities;
  }

  /**
   * @param idTask new value of {@link #getactvities}.
   */
  public void setActivities(List<ActivityEntity> activities) {

    this.activities = activities;
  }

  @Override
  @Transient
  public Long getCustomerId() {

    if (this.customer == null) {
      return null;
    }
    return this.customer.getId();
  }

  @Override
  public void setCustomerId(Long customerId) {

    if (customerId == null) {
      this.customer = null;
    } else {
      CustomerEntity customerEntity = new CustomerEntity();
      customerEntity.setId(customerId);
      this.customer = customerEntity;
    }
  }

  @Override
  @Transient
  public Long getStatusId() {

    if (this.status == null) {
      return null;
    }
    return this.status.getId();
  }

  @Override
  public void setStatusId(Long statusId) {

    if (statusId == null) {
      this.status = null;
    } else {
      ItineraryStatusEntity itineraryStatusEntity = new ItineraryStatusEntity();
      itineraryStatusEntity.setId(statusId);
      this.status = itineraryStatusEntity;
    }
  }

}
