package com.cap.maas.itinerarymanagement.logic.api.to;

import java.util.List;

import com.cap.maas.general.common.api.to.AbstractCto;

/**
 * Composite transport object of Customer
 */
public class CustomerCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private CustomerEto customer;

  private List<ItineraryEto> itineraries;

  public CustomerEto getCustomer() {

    return customer;
  }

  public void setCustomer(CustomerEto customer) {

    this.customer = customer;
  }

  public List<ItineraryEto> getItineraries() {

    return itineraries;
  }

  public void setItineraries(List<ItineraryEto> itineraries) {

    this.itineraries = itineraries;
  }

}
