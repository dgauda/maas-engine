package com.cap.maas.itinerarymanagement.service.impl.rest;

import javax.inject.Inject;
import javax.inject.Named;

import com.cap.maas.itinerarymanagement.logic.api.Itinerarymanagement;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivitySearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityStatusCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityStatusEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityStatusSearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ActivityTypeSearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.CustomerCto;
import com.cap.maas.itinerarymanagement.logic.api.to.CustomerEto;
import com.cap.maas.itinerarymanagement.logic.api.to.CustomerSearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItinerarySearchCriteriaTo;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryStatusCto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryStatusEto;
import com.cap.maas.itinerarymanagement.logic.api.to.ItineraryStatusSearchCriteriaTo;
import com.cap.maas.itinerarymanagement.service.api.rest.ItinerarymanagementRestService;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * The service implementation for REST calls in order to execute the logic of component {@link Itinerarymanagement}.
 */
@Named("ItinerarymanagementRestService")
public class ItinerarymanagementRestServiceImpl implements ItinerarymanagementRestService {

  @Inject
  private Itinerarymanagement itinerarymanagement;

  @Override
  public ItineraryEto getItinerary(long id) {

    return this.itinerarymanagement.findItinerary(id);
  }

  @Override
  public PaginatedListTo<ItineraryEto> getAllItineraries() {

    ItinerarySearchCriteriaTo searchCriteriaTo = new ItinerarySearchCriteriaTo();
    return this.itinerarymanagement.findItineraryEtos(searchCriteriaTo);
  }

  @Override
  public ItineraryEto saveItinerary(ItineraryEto itinerary) {

    return this.itinerarymanagement.saveItinerary(itinerary);
  }

  @Override
  public void deleteItinerary(long id) {

    this.itinerarymanagement.deleteItinerary(id);
  }

  @Override
  public PaginatedListTo<ItineraryEto> findItinerarysByPost(ItinerarySearchCriteriaTo searchCriteriaTo) {

    return this.itinerarymanagement.findItineraryEtos(searchCriteriaTo);
  }

  @Override
  public ItineraryCto getItineraryCto(long id) {

    return this.itinerarymanagement.findItineraryCto(id);
  }

  @Override
  public ItineraryEto saveItinerary(ItineraryCto itinerary) {

    return this.itinerarymanagement.saveItinerary(itinerary);
  }

  @Override
  public PaginatedListTo<ItineraryCto> findItineraryCtosByPost(ItinerarySearchCriteriaTo searchCriteriaTo) {

    return this.itinerarymanagement.findItineraryCtos(searchCriteriaTo);
  }

  @Override
  public ActivityEto getActivity(long id) {

    return this.itinerarymanagement.findActivity(id);
  }

  @Override
  public ActivityEto saveActivity(ActivityEto activity) {

    return this.itinerarymanagement.saveActivity(activity);
  }

  @Override
  public void deleteActivity(long id) {

    this.itinerarymanagement.deleteActivity(id);
  }

  @Override
  public PaginatedListTo<ActivityEto> findActivitysByPost(ActivitySearchCriteriaTo searchCriteriaTo) {

    return this.itinerarymanagement.findActivityEtos(searchCriteriaTo);
  }

  @Override
  public ActivityCto getActivityCto(long id) {

    return this.itinerarymanagement.findActivityCto(id);
  }

  @Override
  public PaginatedListTo<ActivityCto> findActivityCtosByPost(ActivitySearchCriteriaTo searchCriteriaTo) {

    return this.itinerarymanagement.findActivityCtos(searchCriteriaTo);
  }

  @Override
  public ActivityTypeEto getActivityType(long id) {

    return this.itinerarymanagement.findActivityType(id);
  }

  @Override
  public ActivityTypeEto saveActivityType(ActivityTypeEto activitytype) {

    return this.itinerarymanagement.saveActivityType(activitytype);
  }

  @Override
  public void deleteActivityType(long id) {

    this.itinerarymanagement.deleteActivityType(id);
  }

  @Override
  public PaginatedListTo<ActivityTypeEto> findActivityTypesByPost(ActivityTypeSearchCriteriaTo searchCriteriaTo) {

    return this.itinerarymanagement.findActivityTypeEtos(searchCriteriaTo);
  }

  @Override
  public ActivityTypeCto getActivityTypeCto(long id) {

    return this.itinerarymanagement.findActivityTypeCto(id);
  }

  @Override
  public PaginatedListTo<ActivityTypeCto> findActivityTypeCtosByPost(ActivityTypeSearchCriteriaTo searchCriteriaTo) {

    return this.itinerarymanagement.findActivityTypeCtos(searchCriteriaTo);
  }

  @Override
  public ActivityStatusEto getActivityStatus(long id) {

    return this.itinerarymanagement.findActivityStatus(id);
  }

  @Override
  public ActivityStatusEto saveActivityStatus(ActivityStatusEto activitystatus) {

    return this.itinerarymanagement.saveActivityStatus(activitystatus);
  }

  @Override
  public void deleteActivityStatus(long id) {

    this.itinerarymanagement.deleteActivityStatus(id);
  }

  @Override
  public PaginatedListTo<ActivityStatusEto> findActivityStatussByPost(ActivityStatusSearchCriteriaTo searchCriteriaTo) {

    return this.itinerarymanagement.findActivityStatusEtos(searchCriteriaTo);
  }

  @Override
  public ActivityStatusCto getActivityStatusCto(long id) {

    return this.itinerarymanagement.findActivityStatusCto(id);
  }

  @Override
  public PaginatedListTo<ActivityStatusCto> findActivityStatusCtosByPost(
      ActivityStatusSearchCriteriaTo searchCriteriaTo) {

    return this.itinerarymanagement.findActivityStatusCtos(searchCriteriaTo);
  }

  @Override
  public void resetActivity(Long id) {

    this.itinerarymanagement.reset(id);
  }

  @Override
  public ActivityEto bookActivity(long id, long servId) {

    return this.itinerarymanagement.book(id, servId);

  }

  @Override
  public CustomerEto getCustomer(long id) {

    return this.itinerarymanagement.findCustomer(id);
  }

  @Override
  public CustomerEto saveCustomer(CustomerEto customer) {

    return this.itinerarymanagement.saveCustomer(customer);
  }

  @Override
  public void deleteCustomer(long id) {

    this.itinerarymanagement.deleteCustomer(id);
  }

  @Override
  public PaginatedListTo<CustomerEto> findCustomersByPost(CustomerSearchCriteriaTo searchCriteriaTo) {

    return this.itinerarymanagement.findCustomerEtos(searchCriteriaTo);
  }

  @Override
  public CustomerCto getCustomerCto(long id) {

    return this.itinerarymanagement.findCustomerCto(id);
  }

  @Override
  public PaginatedListTo<CustomerCto> findCustomerCtosByPost(CustomerSearchCriteriaTo searchCriteriaTo) {

    return this.itinerarymanagement.findCustomerCtos(searchCriteriaTo);
  }

  @Override
  public ItineraryStatusEto getItineraryStatus(long id) {

    return this.itinerarymanagement.findItineraryStatus(id);
  }

  @Override
  public ItineraryStatusEto saveItineraryStatus(ItineraryStatusEto itinerarystatus) {

    return this.itinerarymanagement.saveItineraryStatus(itinerarystatus);
  }

  @Override
  public void deleteItineraryStatus(long id) {

    this.itinerarymanagement.deleteItineraryStatus(id);
  }

  @Override
  public PaginatedListTo<ItineraryStatusEto> findItineraryStatussByPost(
      ItineraryStatusSearchCriteriaTo searchCriteriaTo) {

    return this.itinerarymanagement.findItineraryStatusEtos(searchCriteriaTo);
  }

  @Override
  public ItineraryStatusCto getItineraryStatusCto(long id) {

    return this.itinerarymanagement.findItineraryStatusCto(id);
  }

  @Override
  public PaginatedListTo<ItineraryStatusCto> findItineraryStatusCtosByPost(
      ItineraryStatusSearchCriteriaTo searchCriteriaTo) {

    return this.itinerarymanagement.findItineraryStatusCtos(searchCriteriaTo);
  }

}
