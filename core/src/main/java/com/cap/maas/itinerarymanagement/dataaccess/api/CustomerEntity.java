package com.cap.maas.itinerarymanagement.dataaccess.api;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cap.maas.general.dataaccess.api.ApplicationPersistenceEntity;
import com.cap.maas.itinerarymanagement.common.api.Customer;

/**
 * @author sohrshai
 */
@Entity
@Table(name = "Customer")
public class CustomerEntity extends ApplicationPersistenceEntity implements Customer {

  private String name;

  private int contactnumber;

  private boolean isregularcustomer;

  @Temporal(TemporalType.TIMESTAMP)
  private Timestamp registrationDate;

  private List<ItineraryEntity> itineraries;

  private static final long serialVersionUID = 1L;

  /**
   * @return itineraries
   */
  @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
  public List<ItineraryEntity> getItineraries() {

    return this.itineraries;
  }

  /**
   * @param itineraries new value of {@link #getitineraries}.
   */
  public void setItineraries(List<ItineraryEntity> itineraries) {

    this.itineraries = itineraries;
  }

  /**
   * @return name
   */
  public String getName() {

    return this.name;
  }

  /**
   * @param name new value of {@link #getname}.
   */
  public void setName(String name) {

    this.name = name;
  }

  /**
   * @return contactnumber
   */
  public int getContactnumber() {

    return this.contactnumber;
  }

  /**
   * @param contactnumber new value of {@link #getcontactnumber}.
   */
  public void setContactnumber(int contactnumber) {

    this.contactnumber = contactnumber;
  }

  /**
   * @return isregularcustomer
   */
  public boolean isIsregularcustomer() {

    return this.isregularcustomer;
  }

  /**
   * @param isregularcustomer new value of {@link #getisregularcustomer}.
   */
  public void setIsregularcustomer(boolean isregularcustomer) {

    this.isregularcustomer = isregularcustomer;
  }

  /**
   * @return registrationDate
   */
  public Timestamp getRegistrationDate() {

    return this.registrationDate;
  }

  /**
   * @param registrationDate new value of {@link #getregistrationDate}.
   */
  public void setRegistrationDate(Timestamp registrationDate) {

    this.registrationDate = registrationDate;
  }

}
