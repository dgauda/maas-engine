    CREATE TABLE ItineraryStatus ( 
      id BIGINT NOT NULL AUTO_INCREMENT,  
      modificationCounter INTEGER NOT NULL,
      name VARCHAR(255),
      desc VARCHAR(2000),
      CONSTRAINT PK_ItineraryStatus PRIMARY KEY(id)      
    );
    
    CREATE TABLE Itinerary ( 
      id BIGINT NOT NULL AUTO_INCREMENT,  
      modificationCounter INTEGER NOT NULL,     
      name VARCHAR(255),
      Desc VARCHAR(2000),
      creationDate TIMESTAMP,
      idCustomer BIGINT,
      idItinStatus BIGINT,
      CONSTRAINT PK_Itinerary PRIMARY KEY(id),  
      CONSTRAINT FK_Itinerary_idItinStatus FOREIGN KEY(idItinStatus) REFERENCES ItineraryStatus(id) NOCHECK

            
    );
    
    CREATE TABLE ActivityType ( 
      id BIGINT NOT NULL AUTO_INCREMENT,  
      modificationCounter INTEGER NOT NULL,
      name VARCHAR(255),
      active BOOLEAN,
      CONSTRAINT PK_ActivityType PRIMARY KEY(id)      
    );
    
    CREATE TABLE ActivityStatus ( 
      id BIGINT NOT NULL AUTO_INCREMENT,  
      modificationCounter INTEGER NOT NULL,
      name VARCHAR(255),
      desc VARCHAR(2000),
      CONSTRAINT PK_ActivityStatus PRIMARY KEY(id)      
    );
    

    
    CREATE TABLE ServiceRegistration ( 
      id BIGINT NOT NULL AUTO_INCREMENT,  
      modificationCounter INTEGER NOT NULL,
      idActivityType BIGINT NOT NULL,
      name VARCHAR(255),
      desc VARCHAR(2000),
      latitude DOUBLE,
      longitude DOUBLE,
      serviceUrl VARCHAR(2000),
      country VARCHAR(255),
      city VARCHAR(255),
      area VARCHAR(3000),
      CONSTRAINT PK_ServiceRegistration PRIMARY KEY(id),
      CONSTRAINT FK_ServiceRegistration_idActivityType FOREIGN KEY(idActivityType) REFERENCES ActivityType(id) NOCHECK      
    );
    
    CREATE TABLE Activity ( 
      id BIGINT NOT NULL AUTO_INCREMENT,  
      modificationCounter INTEGER NOT NULL,
      idItinerary  BIGINT NOT NULL,
      idRegistration BIGINT NOT NULL,
      idActStatus BIGINT NOT NULL,
      name VARCHAR(255),
      desc VARCHAR(2000),
      priority BIGINT,     
      CONSTRAINT PK_Activity PRIMARY KEY(id),
      CONSTRAINT FK_Activity_idRegistration FOREIGN KEY(idRegistration) REFERENCES ServiceRegistration(id) NOCHECK,
      CONSTRAINT FK_Activity_idItinerary FOREIGN KEY(idItinerary) REFERENCES Itinerary(id) NOCHECK,
      CONSTRAINT FK_Activity_idActStatus FOREIGN KEY(idActStatus) REFERENCES ActivityStatus(id) NOCHECK
    );    

	CREATE TABLE Customer ( 
		id BIGINT NOT NULL AUTO_INCREMENT,
		modificationCounter INTEGER NOT NULL, 
		name VARCHAR(255), 
		contactnumber integer, 
		isregularcustomer boolean, 
		registrationDate TIMESTAMP,
		CONSTRAINT PK_Entertainment_Customer PRIMARY KEY(id)
	);
	    
    