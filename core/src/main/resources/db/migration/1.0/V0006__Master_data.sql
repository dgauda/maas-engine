INSERT INTO ActivityStatus (id, modificationCounter, name , desc) values (1, 0, 'PENDING', 'Acivity Pending');
INSERT INTO ActivityStatus (id, modificationCounter, name , desc) values (2, 0, 'COMPLETE', 'Acivity Completed');

INSERT INTO ItineraryStatus (id, modificationCounter, name , desc) values (1, 0, 'PENDING', 'Itinerary Pending');
INSERT INTO ItineraryStatus (id, modificationCounter, name , desc) values (2, 0, 'COMPLETE', 'Itinerary Completed');

INSERT INTO ActivityType (id, modificationCounter, name , active) values (1, 0, 'Parking', true);
INSERT INTO ActivityType (id, modificationCounter, name , active) values (2, 0, 'Entertainment', true);
INSERT INTO ActivityType (id, modificationCounter, name , active) values (3, 0, 'FoodnRetail', true);


INSERT INTO ServiceRegistration (id, modificationCounter, name, desc, idActivityType, latitude, longitude, serviceUrl, country, city, area) VALUES (1, 0, 'WhitefieldParkingService', 'Parking Service : whitefield', 1, 9.1234567, 3.1234567, 'http://parking-simulator.rnd.svc:8081/simulators/services/rest/parkingmanagement/v1/parkinglot/', 'India', 'Bangalore','DTP-whitefield');
INSERT INTO ServiceRegistration (id, modificationCounter, name, desc, idActivityType, latitude, longitude, serviceUrl, country, city, area) VALUES (2, 0, 'WhitefieldEntertainmentService', 'Form Mall Showtimes : whitefield', 2, 9.1234567, 3.1234567, 'http://entertainment-simulator.rnd.svc:8081/simulators/services/rest/entertainmentmanagement/v1/reservation/', 'India', 'Bangalore','Varthur-whitefield');
INSERT INTO ServiceRegistration (id, modificationCounter, name, desc, idActivityType, latitude, longitude, serviceUrl, country, city, area) VALUES (3, 0, 'WhitefieldFoodnRetailService', 'FoodnRetail Service : whitefield', 3, 9.1234567, 3.1234567, 'http://dummyserviceurl/foodnretailservice', 'India', 'Bangalore','Varthur-whitefield');

INSERT INTO ServiceRegistration (id, modificationCounter, name, desc, idActivityType, latitude, longitude, serviceUrl, country, city, area) VALUES (4, 0, 'ConcordeParkingService', 'Parking Service : Paris France', 1, 9.1234567, 3.1234567, 'http://parking-simulator.rnd.svc:8081/simulators/services/rest/parkingmanagement/v1/parkinglot/', 'France', 'Paris','Place-de-la-Concorde');
INSERT INTO ServiceRegistration (id, modificationCounter, name, desc, idActivityType, latitude, longitude, serviceUrl, country, city, area) VALUES (5, 0, 'EiffelTowerShow', 'EiffelTowerShow', 2, 9.1234567, 3.1234567, 'http://entertainment-simulator.rnd.svc:8081/simulators/services/rest/entertainmentmanagement/v1/reservation/', 'France', 'Paris', 'Avenue-Anatole');
INSERT INTO ServiceRegistration (id, modificationCounter, name, desc, idActivityType, latitude, longitude, serviceUrl, country, city, area) VALUES (6, 0, 'CitadinesHotelParkingService', 'Parking Service : Paris France', 1, 9.1234567, 3.1234567, 'http://parking-simulator.rnd.svc:8081/simulators/services/rest/parkingmanagement/v1/parkinglot/', 'France', 'Paris','Tilsitt');

-- new table for payload containing url and payload json format

-- new table for customer entity
INSERT INTO Customer(id, modificationCounter, name, contactnumber, isregularcustomer, registrationdate) VALUES (1001, 0, 'Mark', '3242424', 'Y', '2018-10-11');
INSERT INTO Customer(id, modificationCounter, name, contactnumber, isregularcustomer, registrationdate) VALUES (1002, 0, 'John', '123123', 'Y', '2019-02-12');

INSERT INTO Itinerary (id, modificationCounter, name, desc, creationDate, idCustomer, idItinStatus) VALUES (1, 1, 'GoaTrip', 'Blr to Goa', CURRENT_TIMESTAMP + (60 * 60 * 24 * 5), 1001, 1);
INSERT INTO Itinerary (id, modificationCounter, name, desc, creationDate, idCustomer, idItinStatus) VALUES (2, 1, 'GoaTrip', 'Blr to Goa', CURRENT_TIMESTAMP + (60 * 60 * 24 * 5), 1001, 1);

INSERT INTO Activity (id, modificationCounter, name, desc, idRegistration, idActStatus, priority, idItinerary) VALUES (1, 1, 'Book Parking', 'Book Parking space', 1 ,1, 1, 1);
INSERT INTO Activity (id, modificationCounter, name, desc, idRegistration, idActStatus, priority, idItinerary) VALUES (2, 1, 'Book Entertainment', 'Book Entertainment ticket', 2, 1, 2, 1);
